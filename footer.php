<?php /*
THE TEMPLATE FOR DISPLAYING UNIVERSAL FOOTER
*/ ?>
	<div class="pop-dark">
		<div class="pop-up">
			<div class="close-pop-up"></div>
			<h2>To View this Resource</h2>
			<p>A quick registration is required to view our resources.<br/>You will only be asked to do this one time (unless you don't save your browser cookies).</p>
			<h4>All form fields are required.</h4>
			<?php switch_to_blog(1); ?>
				<?php echo do_shortcode( '[gravityform id="6" title="false" description="false"]' ); ?>
			<?php restore_current_blog(); ?>
		</div>	
	</div>			
	<footer id="footer">
		<div class="max-width">
			<div class="one-fourth">
				<h2>Client Specializations</h2>
				<hr>
				<?php switch_to_blog(1); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'specialization-nav-footer' ) ); ?>
				<?php restore_current_blog(); ?>
				<div style="clear: both"></div>
			</div>
			<div class="one-fourth">
				<h2>Service Industry Groups</h2>
				<hr>
				<?php switch_to_blog(1); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'services-nav-footer' ) ); ?>
				<?php restore_current_blog(); ?>
				<div style="clear: both"></div>
			</div>
			<div class="responsive-clear"></div>
			<div class="one-fourth">
				<h2>Press</h2>
				<hr>
				<?php switch_to_blog(1); ?>
					<?php $args = array(
					    'posts_per_page' => 2,
					    'offset' => 0,
					    'post_type' => 'press'
					);

					$the_query = new WP_Query( $args );
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'press-footer' );
						}
					} ?>
					<?php wp_reset_query(); ?>
				<?php restore_current_blog(); ?>
				<h2 style="margin-top: 24px;">Awards</h2>
				<hr>
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('footer-sidebar')) : else : ?>
					<p><strong>Widget Ready</strong></p>  
				<?php endif; ?> 
			</div>
			<div class="one-fourth">
				<h2>Connect</h2>
				<hr>
				<div id="social-container">
					<a target="_blank" href="https://www.facebook.com/DoerenMayhewCPA/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-facebook.svg" /></a>
					<a target="_blank" href="https://twitter.com/DoerenMayhew"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-twitter.svg" /></a>
					<a target="_blank" href="https://www.linkedin.com/company/doeren-mayhew"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-linkedin.svg" /></a>
					<a target="_blank" href="https://www.youtube.com/user/doerenmayhew"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-youtube.svg" /></a>
					<a target="_blank" href="https://www.instagram.com/doerenmayhew/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-instagram-blue.svg" /></a>
				</div>
				<h2>Quick Contact</h2>
				<hr>
				<?php switch_to_blog(1); ?>
					<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
				<?php restore_current_blog(); ?>
			</div>
			<div style="clear: both"></div>
			<div class="legal-contents">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/legal-footer-img.png" />
				<?php echo do_shortcode('[widget id="text-31"]'); ?>
			</div>
		</div>	
	</footer>
	<div class="copyright full-width">
		<div class="max-width">
			<div class="contents">
				�Copyright <?php echo date('Y'); ?> Doeren Mayhew. All Rights Reserved. 
				<a href="/privacy-policy/">Privacy Policy</a> | <a href="/terms-of-use/">Terms of Use</a> | <a target="_blank" href="https://element5digital.com/">Design Element5</a>
			</div>
			<div style="clear: both"></div>
		</div>
	</div>

<!-- Databreach Calculator JS -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/data_breach_calculator.js"></script>
<!-- Cookie Banner -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/cookie-banner.js"></script>
<!-- Cookie -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/cookie.js"></script>
<!-- Sticky Navigation -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/stickynav.js"></script>
<!-- Mobile Navigation -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/mobilenav.js"></script>
<!-- Smooth Scrolling -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.js"></script>
<!-- Entrance Animations -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/entranceanimation.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.viewportchecker.min.js"></script>
	<script type="text/javascript">
		jQuery(function($){
			$('.pop-up-form-read-more').click(function(){
				$('#input_6_5_6').val($(this).attr('data-link'));
			});
			jQuery(document).on('click', '.pop-up-form-read-more', function(){
				$('#input_6_5_6').val($(this).attr('data-link'));
			});
		});
	</script>
<!-- Mega Menu -->
	<script>
		jQuery('#menu-item-147').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#services-sub-menu').removeClass('open');	
		});
		jQuery('#menu-item-147').mouseenter(function(){
			jQuery('#services-sub-menu').addClass('open');
			jQuery('#menu-item-147').css('background-color', '#fff');
		});
		jQuery('#services-sub-menu').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#services-sub-menu').removeClass('open');	
		});
		jQuery('#services-sub-menu').mouseenter(function(){
			jQuery('#services-sub-menu').addClass('open');
			jQuery('#menu-item-147').css('background-color', '#fff');
		});
	</script>
	<script>
		jQuery('#menu-item-148').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#specializations-sub-menu').removeClass('open');	
		});
		jQuery('#menu-item-148').mouseenter(function(){
			jQuery('#specializations-sub-menu').addClass('open');
			jQuery('#menu-item-148').css('background-color', '#fff');
		});
		jQuery('#specializations-sub-menu').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#specializations-sub-menu').removeClass('open');	
		});
		jQuery('#specializations-sub-menu').mouseenter(function(){
			jQuery('#specializations-sub-menu').addClass('open');
			jQuery('#menu-item-148').css('background-color', '#fff');
		});
	</script>
	<script>
		jQuery('#menu-item-149').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#resources-sub-menu').removeClass('open');	
		});
		jQuery('#menu-item-149').mouseenter(function(){
			jQuery('#resources-sub-menu').addClass('open');
			jQuery('#menu-item-149').css('background-color', '#fff');
		});
		jQuery('#resources-sub-menu').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#resources-sub-menu').removeClass('open');	
		});
		jQuery('#resources-sub-menu').mouseenter(function(){
			jQuery('#resources-sub-menu').addClass('open');
			jQuery('#menu-item-149').css('background-color', '#fff');
		});
	</script>
	<script>
		jQuery('#menu-item-150').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#blog-sub-menu').removeClass('open');	
		});
		jQuery('#menu-item-150').mouseenter(function(){
			jQuery('#blog-sub-menu').addClass('open');
			jQuery('#menu-item-150').css('background-color', '#fff');
		});
		jQuery('#blog-sub-menu').mouseleave(function(){
			jQuery('#menu-main-navigation .menu-item').css('background-color', 'transparent');
			jQuery('#blog-sub-menu').removeClass('open');	
		});
		jQuery('#blog-sub-menu').mouseenter(function(){
			jQuery('#blog-sub-menu').addClass('open');
			jQuery('#menu-item-150').css('background-color', '#fff');
		});
	</script>
<!-- Secondary Nav Childer -->
	<script>
		jQuery('.secondary-nav nav li.menu-item-has-children').mouseleave(function(){
			jQuery(this).children('ul').removeClass('open');	
		});
		jQuery('.secondary-nav nav li.menu-item-has-children').mouseenter(function(){
			jQuery(this).children('ul').addClass('open');	
		});
		jQuery('.secondary-nav nav li.menu-item-has-children ul').mouseleave(function(){
			jQuery(this).removeClass('open');	
		});
		jQuery('.secondary-nav nav li.menu-item-has-children ul').mouseenter(function(){
			jQuery(this).addClass('open');
		});
	</script>
<!-- Press Page Filter -->
	<script>
		jQuery('#current-year-cta').click(function(){
			jQuery('#old-posts').removeClass('show');
			jQuery('#load-more-old-press').removeClass('show');
			jQuery('#current-year').addClass('show');
			jQuery('#load-more-press').addClass('show');
		});
	</script>
	<script>
		jQuery('#old-posts-cta').click(function(){
			jQuery('#old-posts').addClass('show');
			jQuery('#load-more-old-press').addClass('show');
			jQuery('#current-year').removeClass('show');
			jQuery('#load-more-press').removeClass('show');
		});
	</script>
<!-- Carousel Lead Contacts -->
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flipster.min.js"></script>  
	<script> 
		jQuery(document).ready(function(){ 
			jQuery('.my-flipster').flipster({ 
				loop: true,
				spacing: -0.75,
				click: false,
				buttons: true,
			});
		}) 
	</script>
<!-- Archieves Drop Downs -->
	<script>
		jQuery('#accordion_archives-2 ul li').click(function(){
			jQuery('#accordion_archives-3 ul').css('display', 'none');
			jQuery('#accordion_archives-3 ul ul').css('display', 'none');
		});
		jQuery("#older-archieves").click(function(){
		    jQuery('#accordion_archives-3 ul').css('display', 'block');
			jQuery('#accordion_archives-3 ul ul').css('display', 'none');
			jQuery('#accordion_archives-2 ul ul').css('display', 'none');
		});
		jQuery('#accordion_archives-4 ul li').click(function(){
			jQuery('#accordion_archives-3 ul').css('display', 'none');
			jQuery('#accordion_archives-3 ul ul').css('display', 'none');
		});
		jQuery("#older-archieves").click(function(){
		    jQuery('#accordion_archives-3 ul').css('display', 'block');
			jQuery('#accordion_archives-3 ul ul').css('display', 'none');
			jQuery('#accordion_archives-4 ul ul').css('display', 'none');
		});
	</script>
<!-- Resource Form -->
	<script>
		jQuery('.pop-up-form').click(function(){
			jQuery('.pop-up').addClass('pop');
			jQuery('.pop-dark').addClass('pop');
		});
		jQuery('.close-pop-up').click(function(){
			jQuery('.pop-up').removeClass('pop');
			jQuery('.pop-dark').removeClass('pop');
		});
		// jQuery(document).ready(function($){
			jQuery(document).on('click', '.pop-up-form', function(){
				jQuery('.pop-up').addClass('pop');
				jQuery('.pop-dark').addClass('pop');
			});
			jQuery(document).on('click', '.close-pop-up', function(){
				jQuery('.pop-up').removeClass('pop');
				jQuery('.pop-dark').removeClass('pop');
			});
		// });
	</script>
	<script>
		jQuery(document).ready(function(){
		  jQuery('.mega-menu a').attr('target', '_self');
		});
	</script>
	
	<!-- Start of HubSpot Embed Code -->
	<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/20040822.js"></script>
	<!-- End of HubSpot Embed Code -->
	
</body>

<?php wp_footer(); ?>

</html>