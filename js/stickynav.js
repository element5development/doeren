/* Sticky Navigation Entrance */

jQuery(document).ready(function($){
	$(window).scroll(function() {    
		var scroll = $(window).scrollTop();

	    if (scroll >= 150)  
	    {   
	        $('#sticky-nav').css({
	            'opacity' : '1',
	            'z-index' : '1000'
	        });
	    } 
	    else
	    {   
	        $('#sticky-nav').css({
	            'opacity' : '0',
	            'z-index' : '-1'
	        });
	    }
	})
});
