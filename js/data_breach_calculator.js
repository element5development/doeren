// var $ = jQuery;

// $(document).ready(function () {

var expanded = false;

function showCheckboxes() {
	var checkboxes = document.getElementById("checkboxes");
	if (!expanded) {
		checkboxes.style.display = "block";
		document.getElementById("multiselectText").innerHTML = "Click to Close Dropdown";
		expanded = true;
	} else {
		checkboxes.style.display = "none";
		document.getElementById("multiselectText").innerHTML = "Choose Factors";
		expanded = false;
	}
}

/*
 *	churnRateand costPerRecord are both being initialized here. They are kept outside of the 
 *	updateIndustry() function that they are changed in to prevent any scoping issues when it
 *	comes time to calculate the final cost of the data breach.
 */
var churnRate = 1;
var costPerRecord = -1;
var upperEndCost = 30000000;
/*
 *	This 2d array is responsible for feeding the for loops in the calculate() function that
 *	incorporate the concept of dimishing cost with the records. Dimishing costs must be taken 
 *	into account due to discounts that would be avaiable on purchasing services from firms in
 *	bulk, as well as the tapering off of any fixed costs per record as more and more records
 *	become involved in the situation. 
 *
 *	The first variable in each entry represents the upper limit of the range that that specifc
 *	entry is representing. For example, the first entry will effect entries 0 - 10,000, and the 
 *	last entry will represent anywhere between 1,000,000 and 100,000,000 stolen entries.
 *
 *	The second variable in each entry represents the weight that will be allocated to each range
 *	of entries, with the cost per record falling off steeply as the number of records increases.
 *	for example, any record between 0 and 25,000 will cost the company the full expected cost,
 *	whereas any record leaked between 1,000,0000 and 100,000,000 will only cost the company 3.5%
 *	of the expected cost.
 *
 *	It should be noted that the multiples in this chart were selected based off of the average 
 *	cost of a breach in regards to size, as given by the 2017 Ponemon Insitute cost of Data 
 *	Breach study that was conducted in the U.S.
 */
var diminishingCostsTable = [
	[10000, 1.3],
	[17500, 1.05],
	[25000, .5],
	[37500, .14],
	[50000, .1],
	[100000, .2],
	[200000, .1],
	[1000000, .035],
	[10000000000, .01]
];
/*
 *	Designed to work with the other significant factors overlay. This significantly shortens the
 *	length of the code by enabling the entire list of checkboxes to be processed by a single for
 *	loop. The first column of the table correspons to the IDs of the items within the list, whereas
 *	the second column of numbers within this array matches up those items with the cost impact that
 *	is associated with them, given by the Ponemon study.
 */
var factorsTable = [
	["factor1", 25.9],
	["factor2", 22.5],
	["factor3", 16.8],
	["factor4", 15.4],
	["factor5", 9.9],
	["factor6", 9.3],
	["factor7", 8.2],
	["factor8", 8.2],
	["factor9", 7.9],
	["factor10", 7.7],
	["factor11", 6.1],
	["factor12", 5.0],
	["factor13", 4.3]
];
var numberOfRecords = 0;
//Starts the user off viewing the cost of a data breach that involved 10,000 records.
document.getElementById("recordsLeaked").value = 10000;
//Included so that the abnormal churn rate is displayed from the start.
updateIndustry();
/*
 *	This function is called whenever the select element with the ID "industry" is changed.
 *	Whenever the industry is updated, the switch loop that is contained within this function
 *	will change the cost per record, as well as the new churn rate that is associated with 
 *	the industry that the user has chosen from the drop down menu.
 *
 *	All values in the switch loop are hardcoded and can be changed by the developer in order
 *	to reflect any updated information in order to keep this data breach calculator relevant.
 */
function updateIndustry() {
	switch (document.getElementById("industry").value) {
		case "health":
			costPerRecord = 408;
			churnRate = 5.5;
			break;
		case "financial":
			costPerRecord = 336;
			churnRate = 7.1;
			break;
		case "services":
			costPerRecord = 274;
			churnRate = 4.7;
			break;
		case "lifeScience":
			costPerRecord = 264;
			churnRate = 5.7;
			break;
		case "industrial":
			costPerRecord = 259;
			churnRate = 2.4;
			break;
		case "technology":
			costPerRecord = 251;
			churnRate = 5.1;
			break;
		case "education":
			costPerRecord = 245;
			churnRate = 1.8;
			break;
		case "transportation":
			costPerRecord = 250;
			churnRate = 3.2;
			break;
		case "communications":
			costPerRecord = 239;
			churnRate = 1.8;
			break;
		case "energy":
			costPerRecord = 228;
			churnRate = 4.1;
			break;
		case "consumer":
			costPerRecord = 196;
			churnRate = 3.5;
			break;
		case "retail":
			costPerRecord = 177;
			churnRate = 1.9;
			break;
		case "hospitality":
			costPerRecord = 144;
			churnRate = 2.8;
			break;
		case "entertainment":
			costPerRecord = 131;
			churnRate = 1.0;
			break;
		case "research":
			costPerRecord = 123;
			churnRate = 2.0;
			break;
		case "publicSector":
			costPerRecord = 110;
			churnRate = .1;
			break;
		case "other":
			costPerRecord = 225;
			churnRate = 3.2;
			break;
	}

	/*
	 *	Updates the churn rate displayed on the webpage in order to give the user an idea of
	 *	how much business they will lose if they are a victim of a data breach.
	 */
	//	document.getElementById("displayChurn").innerHTML = churnRate + "%";
	calculate();
}

/*
 *	Performs all of the calculations that will be done in the process of finding the total
 * 	cost of the data breach.
 *
 *	The process is as follows:
 *
 *	The cost per record that is associated with the user-selected industry is multiplied by
 *	a factor that is dependent on the cause of the breach, in order to better represent the
 *	data that was given in the Ponemon Insitute Cost of Data Breach Study within the U.S.
 *	The study highlighted that the cost that a company can incur from a breach can vary
 *	steeply based on the cause of the breach/loss.
 *
 *	After the factor is applied, a single for loop then processes all of the check boxes
 *	that the user had selected under the other significant factors dropdown. It uses
 *	the first column of the factorsTable array in order to know where to get each checkbox
 *	input from. The second column of that 2D array is then added to the cost per record if 
 *	the checkbox that was found was selected by the user.
 *
 *	The lowest possible cost is set at $90 per record, which is to prevent impossibly low
 *	results that can be obtained due to the nature of the way the final cost is calculated.
 *
 *	A series of IF statements then calculates the weight of each and every record that was 
 *	leaked. Each record is marginally cheaper than the last due to economies of scale, etc.
 *	
 */
function calculate() {
	var cost = 0;
	if (document.getElementById("recordsLeaked").value < 5000) {
		document.getElementById("recordsLeaked").value = 5000;
	}
	numberOfRecords = document.getElementById("recordsLeaked").value;

	/*
	 *	This variable is in place so that the costPerRecord variable won't get overwritten
	 *	thus reducing the overall likelihood of errors.
	 */
	var finalcost = costPerRecord;
	for (i = 0; i < 12; i++) {
		if (document.getElementById(factorsTable[i][0]).checked == true) {
			finalcost -= factorsTable[i][1];
		}
	}
	finalcost = Math.max(finalcost, 50);

	/*
	 *	Utilizes the diminishingCostsTable array that was defined above in order to 
	 *	properly weight the amount of records that were leaked due to reasons that
	 *	were described above. The weighted total is then multiplied by the cost per
	 *	record.
	 */
	cost = Math.min(numberOfRecords, diminishingCostsTable[0][0]) * diminishingCostsTable[0][1];
	if (numberOfRecords >= diminishingCostsTable[0][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[1][0]) -
			diminishingCostsTable[0][0]) * diminishingCostsTable[1][1];
	}
	if (numberOfRecords >= diminishingCostsTable[1][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[2][0]) -
			diminishingCostsTable[1][0]) * diminishingCostsTable[2][1];
	}
	if (numberOfRecords >= diminishingCostsTable[2][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[3][0]) -
			diminishingCostsTable[2][0]) * diminishingCostsTable[3][1];
	}
	if (numberOfRecords >= diminishingCostsTable[3][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[4][0]) -
			diminishingCostsTable[3][0]) * diminishingCostsTable[4][1];
	}
	if (numberOfRecords >= diminishingCostsTable[4][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[5][0]) -
			diminishingCostsTable[4][0]) * diminishingCostsTable[5][1];
	}
	if (numberOfRecords >= diminishingCostsTable[5][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[6][0]) -
			diminishingCostsTable[5][0]) * diminishingCostsTable[6][1];
	}
	if (numberOfRecords >= diminishingCostsTable[6][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[7][0]) -
			diminishingCostsTable[6][0]) * diminishingCostsTable[7][1];
	}
	if (numberOfRecords >= diminishingCostsTable[7][0]) {
		cost = cost + (Math.min(numberOfRecords, diminishingCostsTable[8][0]) -
			diminishingCostsTable[7][0]) * diminishingCostsTable[8][1];
	}

	cost = cost * finalcost + 2400000;
	document.getElementById("percent").innerHTML = "$" + (cost).toLocaleString('en', {
		maximumSignificantDigits: 7
	});
	rotateGauge(cost);

	if (cost / upperEndCost < 1) {
		document.getElementById("meter").style.background = getColor(cost / upperEndCost);
		document.getElementById("meter").style.transform = 'rotate(' + cost / upperEndCost * 180 + 'deg)';
	} else {
		document.getElementById("meter").style.background = getColor(1);
		document.getElementById("meter").style.transform = 'rotate(' + 180 + 'deg)';
	}
}

function rotateGauge(degrees) {
	if (degrees > 180) degrees = 180;
	document.getElementById("meter").style.transform = 'rotate(' + degrees + 'deg)';
}

function getColor(value) {
	//value from 0 to 1
	var hue = ((1 - value) * 120).toString(10);
	return ["hsl(", hue, ",100%,50%)"].join("");
}

// });