<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR CUSTOM POST TYPE "PRESS"
*/ ?>

<?php get_header(); ?>

<?php 
	$yearly = $_GET["yearly"]; 
	
	$args = [ 
		'post_type' => 'press', 
		'year' => $yearly,
		'posts_per_page' => -1,
    ];
?>

<main class="full-width">

	<div id="page-contents-container" class="max-width archive-container">
			<?php if ( isset($yearly) /*year archive*/) {
			?>

				<section id="single-sidebar-contents" class="left">
				<h2>Archive: <?php echo $yearly; ?></h2>
				<hr>
			    <?php
					query_posts( $args );
					while ( have_posts() ) : the_post();
				?>
					<?php get_template_part( 'template-parts/content', 'press' ); ?>
				<?php endwhile; ?>
			
			<?php
			} elseif ( have_posts() /*archive posts*/ ) {
			?>

				<section id="single-sidebar-contents" class="archive-category-feed left">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'press' ); ?>
				<?php endwhile; ?>

			<?php
			} else /*no posts found*/ {
			?>
			
				<section id="single-sidebar-contents" class="archive-category-feed left">
				<article>
					<h2>No Press Was Found</h2>
				</article>
				<hr>
				<h2></h2>
				<?php 
					$argss = [ 
						'post_type' => 'press', 
						'order' => 'ASC', 
						'posts_per_page' => 5,
				    ];
					query_posts( $argss );
					if ( have_posts() ) : while ( have_posts() ) : the_post();?>
					<?php get_template_part( 'template-parts/content', 'press' ); ?>
					<?php endwhile; ?><?php endif; ?>
			
			<?php		
			} ?>
			<div style="clear: both"></div>
			<?php the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) ); ?>
		</section>
		<aside id="single-sidebar" class="right widget-area-container press-sidebar">
			<div id="press-achives" class="widget widget_text">
				<h2 class="widget-title">Archives</h2>	
				<?php
					$year_2 = date('Y') - 1;
					$year_3 = date('Y') - 2;
					$year_4 = date('Y') - 3;
					$year_5 = date('Y') - 4;
				?>		
				<a href="/press/?yearly=<?php echo date('Y'); ?>"><div class="year-link">
					<?php echo date('Y'); ?>
				</div></a>
				<a href="/press/?yearly=<?php echo $year_2; ?>"><div class="year-link">
					<?php echo $year_2; ?>
				</div></a>
				<a href="/press/?yearly=<?php echo $year_3; ?>"><div class="year-link">
					<?php echo $year_3; ?>
				</div></a>
				<a href="/press/?yearly=<?php echo $year_4; ?>"><div class="year-link">
					<?php echo $year_4; ?>
				</div></a>
				<a href="/press/?yearly=<?php echo $year_5; ?>"><div class="year-link">
					<?php echo $year_5; ?>
				</div></a>
			</div>
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('press-sidebar')) : else : ?>
				<p><strong>Widget Ready</strong></p>  
			<?php endif; ?>  
		</aside>
		<div style="clear: both"></div>
	</div>
	
</main>

<?php get_footer(); ?>