<?php /*
THE TEMPLATE FOR DISPLAYING 404 PAGES (BROKEN LINKS)
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<div class="page-header max-width" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/search-header.png);">
		<div class="page-header-contents">
			<h1 class="page-title">404 Error</h1>
			<p class="page-desctiption">
				I'm sorry but the page your looking for doesn't seem to exist.
			</p>
		</div>
	</div>


	<section id="single-column-contents" class="error-404 max-width not-found">
		<div class="error-search-form"><?php get_search_form(); ?></div>
	</section>
</main>

<?php get_footer(); ?>
