<?php

//SETUP WORDPRESS & BACKEND
    // REMOVE PARENT WIDGET AREAS
       function remove_parent_theme_widgets(){
         unregister_sidebar( 'sidebar-1' );
         unregister_sidebar( 'sidebar-2' );
         unregister_sidebar( 'sidebar-3' );
       }
       add_action( 'widgets_init', 'remove_parent_theme_widgets', 11 );
    // REMOVE PARENT THEME MENUS
       function remove_default_menu(){
         unregister_nav_menu('primary');
         unregister_nav_menu('social');
       }
       add_action( 'after_setup_theme', 'remove_default_menu', 11 );
    // ADD JQUERY TO WORDPRESS
       if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
       function my_jquery_enqueue() {
          wp_deregister_script('jquery');
          wp_register_script('jquery', "https" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js", false, null);
          wp_enqueue_script('jquery');
       }
    // ADD MENUS FOR NAVIGATION
       function register_menu () {
         register_nav_menu('top-nav', __('Top Navigation'));
         register_nav_menu('main-nav', __('Main Navigation'));
         register_nav_menu('mobile-nav', __('Mobile Navigation'));
         register_nav_menu('specialization-nav', __('Specialization Navigation'));
         register_nav_menu('specialization-nav-footer', __('Footer Specialization Navigation'));
         register_nav_menu('services-nav', __('Services Navigation'));
         register_nav_menu('services-nav-footer', __('Footer Services Navigation'));
         register_nav_menu('error-menu', __('404 Menu'));
         register_nav_menu('about-nav', __('About Navigation'));
         register_nav_menu('career-nav', __('Career Navigation'));
         register_nav_menu('why-doeren-nav', __('Why Doeren Navigation'));
         register_nav_menu('service-accounting-nav', __('Accounting Service Navigation'));
         register_nav_menu('service-internal-audit-nav', __('Internal Audit Service Navigation'));
         register_nav_menu('service-business-advisory-nav', __('Business Advisory Service Navigation'));
         register_nav_menu('service-cfo-nav', __('CFO Service Navigation'));
         register_nav_menu('service-it-sssurance-nav', __('IT Assurance Service Navigation'));
         register_nav_menu('service-mergers-nav', __('Mergers Service Navigation'));
         register_nav_menu('service-tax-nav', __('Tax Service Navigation'));
         register_nav_menu('service-tax-incentives-nav', __('Tax Incentives Service Navigation'));
         register_nav_menu('service-dental-nav', __('Dental Service Navigation'));
         register_nav_menu('financial-institutions-nav', __('Financial Institutions'));
         register_nav_menu('events-nav', __('Events Format Filter'));
         register_nav_menu('resourcess-nav', __('Resources Format Filter'));
         register_nav_menu('valuation-nav', __('Valuation & Litigation Support Navigation'));
       }
       add_action('init', 'register_menu');
    // ALLOW SVG FILES IN WORDPRESS
       function cc_mime_types($mimes) {
         $mimes['svg'] = 'image/svg+xml';
         return $mimes;
       }
       add_filter('upload_mimes', 'cc_mime_types');
    // ALLOW VCF FILES IN WORDPRESS
      function custom_upload_mimes ( $existing_mimes=array() ) {
        // add your extension to the array
        $existing_mimes['vcf'] = 'text/x-vcard';
        return $existing_mimes;
      }
      add_filter('upload_mimes', 'custom_upload_mimes');
    // ALLOW EDITOR ACCESS TO GRAVITY FORMS, MENUS & WIDGETS
       function add_grav_forms(){
         $role = get_role('editor');
         $role->add_cap('gform_full_access');
         $role->add_cap('edit_theme_options');
         $role->add_cap('edit_users');
         $role->add_cap('list_users');
         $role->add_cap('promote_users');
         $role->add_cap('add_users');
         $role->add_cap('create_users');
         $role->add_cap('delete_users');
         $role->add_cap('manage_options');
         $role->add_cap('edit_events');
       }
       add_action('admin_init','add_grav_forms');
    // ENABLE SHORTCODES IN WIDGETS
        add_filter('widget_text', 'do_shortcode');
    // ENABLE BREADCRUMBS TO SITE
       function the_breadcrumb() {
         echo '<ul id="crumbs">';
         if (!is_home()) {
                  echo '<li><a href="';
                  echo get_option('home');
                  echo '">';
                  echo 'Home';
                  echo "</a></li>";
                  if (is_category() || is_single()) {
                       echo '<li>';
                       the_category(' </li><li> ');
                       if (is_single()) {
                                echo "</li><li>";
                                the_title();
                                echo '</li>';
                       }
                  } elseif (is_page()) {
                       echo '<li>';
                       echo the_title();
                       echo '</li>';
                  }
         }
         elseif (is_tag()) {single_tag_title();}
         elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
         elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
         elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
         elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
         elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
         elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
         echo '</ul>';
       }
      //GRAVITY FORM TAB INDEX FIX
        add_filter( 'gform_tabindex', 'gform_tabindexer', 10, 2 );
        function gform_tabindexer( $tab_index, $form = false ) {
            $starting_index = 1000; // if you need a higher tabindex, update this number
            if( $form )
                add_filter( 'gform_tabindex_' . $form['id'], 'gform_tabindexer' );
            return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
        }
      //CHECK IS PAGE IS A CHILD OF PAGE
        function is_child( $page = null )
        {
            global $post;
            // is this even a page?
            if ( ! is_page() )
                return false;
            // does it have a parent?
            if ( ! isset( $post->post_parent ) OR $post->post_parent <= 0 )
                return false;
            // is there something to check against?
            if ( ! isset( $page ) ) {
                // yup this is a sub-page
                return true;
            } else {
                // if $page is an integer then its a simple check
                if ( is_int( $page ) ) {
                    // check
                    if ( $post->post_parent == $page )
                        return true;
                } else if ( is_string( $page ) ) {
                    // get ancestors
                    $parent = get_ancestors( $post->ID, 'page' );
                    // does it have ancestors?
                    if ( empty( $parent ) )
                        return false;
                    // get the first ancestor
                    $parent = get_post( $parent[0] );
                    // compare the post_name
                    if ( $parent->post_name == $page )
                        return true;
                }
                return false;
            }
        }
//SETUP WORDPRESS & BACKEND


//CUSTOMIZE CONTENT FOR CLIENT
    // NUMBER OF RESULTS IN SEARCH
        add_filter('post_limits', 'postsperpage');
        function postsperpage($limits) {
          if (is_search()) {
            global $wp_query;
            $wp_query->query_vars['posts_per_page'] = 21;
          }
          return $limits;
				}
		// DATABREACH CALCULATOR
			function create_databreachcalculato_shortcode() {
				return '
				<div id = "databreach-calculator">
					<div class = "left" id = "leftHalf">
						<div class = "left"> Number of Records Affected </div>
						<div class = "right"><input class = "textField"type = "text" id = "recordsLeaked" autocomplete = "off" onchange = "calculate()" tabindex = "1"></input></div>
						<div class = "left"> Applicable Industry </div>
						<div class = "right">
							<select class = "selector" id = "industry" onchange = "updateIndustry()" tabindex = "2">
								<option value = "health" >Health</option>
								<option value = "financial">Financial</option>
								<option value = "services">Services</option>
								<option value = "lifeScience">Life Science</option>
								<option value = "industrial">Industrial</option>
								<option value = "technology">Technology</option>
								<option value = "education">Education</option>
								<option value = "transportation">Transportation</option>
								<option value = "communications">Communications</option>
								<option value = "energy">Energy</option>
								<option value = "consumer">Consumer</option>
								<option value = "retail">Retail</option>
								<option value = "hospitality">Hospitality</option>
								<option value = "entertainment">Entertainment</option>
								<option value = "research">Research</option>
								<option value = "publicSector">Public Sector</option>
								<option value = "other">Other</option>
							</select>
						</div>
						<div class = "left"> Significant Factors </div>
						<div class = "right">
							<div class = "multiselect">
								<div class = "selectBox" onclick = "showCheckboxes()">
									<select>
										<option id = "multiselectText">Choose Factors</option>
									</select>
									<div class = "overSelect"></div>
								</div>
								<div id = "checkboxes">
									<input type = "checkbox" id = "factor1" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor1">Incident Response Team</label>
									<input type = "checkbox" id = "factor2" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor2">Extensive Use of Encryption</label>
									<input type = "checkbox" id = "factor3" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor3">Employee Training</label>
									<input type = "checkbox" id = "factor4" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor4">Robust Business Continuity Management</label>
									<input type = "checkbox" id = "factor5" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor5">Insurance Protection</label>
									<input type = "checkbox" id = "factor6" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor6">Extensive use of DLP</label>
									<input type = "checkbox" id = "factor7" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor7">Board-level Involvement</label>
									<input type = "checkbox" id = "factor8" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor8">Participation in Threat Sharing</label>
									<input type = "checkbox" id = "factor9" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor9">CISO Appointed</label>
									<input type = "checkbox" id = "factor10" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor10">Use of Security Analytics</label>
									<input type = "checkbox" id = "factor11" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor11">Data Classification Schema</label>
									<input type = "checkbox" id = "factor12" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor12">Provision of ID Protection</label>
									<input type = "checkbox" id = "factor13" class = "sigFactorsCheck" onchange = "calculate()"><label for = "factor13">Chief Process Officer(CPO) Appointed</label>
								</div>
							</div>
						</div>	
					</div>
					<div class = "right">
						<div class="gauge">
							<div class="meter" id = "meter"></div>
						</div>
						<div id = "percent">Plain</div>
					</div>
				</div>
				';
			}
			add_shortcode( 'databreach-calculator', 'create_databreachcalculato_shortcode' );
    // PRIMARY BUTTON SHORTCODE
       function primary_button($atts, $content = null) {
         extract( shortcode_atts( array(
              'target' => '',
              'url' => '#'
         ), $atts ) );
         return '<a target="'.$target.'" href="'.$url.'" class="primary-button">' . do_shortcode($content) . '</a>';
       }
       add_shortcode('primary-btn', 'primary_button');
    // SECONDARY BUTTON SHORTCODE
       function secondary_button($atts, $content = null) {
         extract( shortcode_atts( array(
              'target' => '',
              'url' => '#'
         ), $atts ) );
         return '<a target="'.$target.'" href="'.$url.'" class="secondary-button">' . do_shortcode($content) . '</a>';
       }
       add_shortcode('secondary-btn', 'secondary_button');
    // DARK BUTTON SHORTCODE
       function dark_button($atts, $content = null) {
         extract( shortcode_atts( array(
              'target' => '',
              'url' => '#'
         ), $atts ) );
         return '<a target="'.$target.'" href="'.$url.'" class="dark-button">' . do_shortcode($content) . '</a>';
       }
       add_shortcode('dark-btn', 'dark_button');
    // LEAGAL TEXT SHORTCODE
       function legal_text($atts, $content = null) {
         extract( shortcode_atts( array( ), $atts ) );
         return '<p id="legal-text">' . do_shortcode($content) . '</p>';
       }
       add_shortcode('legal-text', 'legal_text');
    // HIGHLIGHT TEXT SHORTCODE
       function mark_text($atts, $content = null) {
         extract( shortcode_atts( array( ), $atts ) );
         return '<mark>' . do_shortcode($content) . '</mark>';
       }
       add_shortcode('highlight', 'mark_text');
    // ONE TIME RESOURCE FORM
        function set_form_6_complete_cookie() {
          setcookie( 'resource-download', 1, 0, COOKIEPATH, COOKIE_DOMAIN, false, false );
        }
        add_action( 'gform_after_submission_6', 'set_form_6_complete_cookie' );

        function conditional_confirmation($confirmation, $form, $lead, $ajax){
            $confirmation = array('redirect' => $lead['5.6']);
            return $confirmation;
        }
        add_filter('gform_confirmation_6', 'conditional_confirmation', 10, 4);
        
        function pre_submission_handler($form_meta) {
            global $wpdb;
            $saveVars = array("Email");
            $saveArray = array();
            foreach($form_meta["fields"] as $field) {
                $saveArray[$field["label"]] = $_POST["input_" . $field["id"]];
                if (in_array($field["label"], $saveVars)) {
                   setcookie("resource-".$field["label"], $_POST["input_" . $field["id"]], 0, COOKIEPATH, COOKIE_DOMAIN, false, false);
                }
            }
            $wpdb->insert( 
                'wp_resources', 
                array( 
                    'person_detail' => json_encode($saveArray),
                    'person_email' => $saveArray['Email'],
                    'date_time' => strtotime(date('Y-m-d H:i'))
                ), 
                array( 
                    '%s'
                ) 
            );
        }
        add_action("gform_pre_submission_6", "pre_submission_handler");

       add_action( 'admin_enqueue_scripts', 'my_jquery_enqueue2' );
       function my_jquery_enqueue2() {
          wp_register_script('jquery-datetimepicker', "https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js", false, null);
          wp_enqueue_script('jquery-datetimepicker');
       }


        function resources_detail_page() {
            add_menu_page(
                'Resource Tracker',
                'Resource Tracker',
                'exist',
                'resources_detail_page',
                'resources_detail_page_render'
            );
        }
        function resources_detail_page_render(){
            global $wpdb;
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['datetimepicker'] != '' && $_POST['datetimepicker2'] != '') {
              $datetimepicker = strtotime($_POST['datetimepicker']);
              $datetimepicker2 = strtotime($_POST['datetimepicker2']);
              $all_resources = $wpdb->get_results( 'SELECT * FROM wp_resources WHERE date_time >= '.$datetimepicker.' AND date_time <= '.$datetimepicker2.' ORDER BY id DESC', OBJECT );
            } else {
              $all_resources = $wpdb->get_results( 'SELECT * FROM wp_resources ORDER BY id DESC', OBJECT );
            }
        ?>
            <h1>Resource Tracker</h1>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css" />
            <script type="text/javascript" src="https:"></script>
            <script type="text/javascript">
            jQuery(function($){
                $('#datetimepicker').datetimepicker({
                  formatTime:'H:i',
                  formatDate:'Y-m-d',
                  defaultDate:'<?php echo date("Y-m-d") ?>',
                  defaultTime:'00:00',
                  timepickerScrollbar:false
                });
                $('#datetimepicker2').datetimepicker({
                  formatTime:'H:i',
                  formatDate:'Y-m-d',
                  defaultDate:'<?php echo date("Y-m-d") ?>',
                  defaultTime:'00:00',
                  timepickerScrollbar:false
                });

                $('input[value="FILTER"]').click(function(){
                  var d = new Date($('#datetimepicker').val());
                  var d2 = new Date($('#datetimepicker2').val());
                  if (d.getTime() > d2.getTime()) {
                    alert('Please check date again, FROM date should be less than TO date');
                    return false;
                  }
                });
            });
            function fnExcelReport(){
                var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
                var textRange; var j=0;
                tab = document.getElementById('allDataExport'); // id of table

                for(j = 0 ; j < tab.rows.length ; j++) 
                {     
                    tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
                    //tab_text=tab_text+"</tr>";
                }

                tab_text=tab_text+"</table>";
                tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
                tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE "); 

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                {
                    txtArea1.document.open("txt/html","replace");
                    txtArea1.document.write(tab_text);
                    txtArea1.document.close();
                    txtArea1.focus(); 
                    sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
                }  
                else                 //other browser not tested on IE 11
                    sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

                return (sa);
            }
            </script>
            <iframe id="txtArea1" style="display:none"></iframe>
            <form method="post" action="" style="float: left;">
              <input type="text" id="datetimepicker" name="datetimepicker" placeholder="From" required>
              <input type="text" id="datetimepicker2" name="datetimepicker2" placeholder="To" required>
              <input type="submit" name="submit" value="FILTER"> <?php if (isset($datetimepicker) && isset($datetimepicker2)): ?>
                Showing results from <strong><?php echo date("m-d-Y H:i:s", $datetimepicker); ?></strong> to <strong><?php echo date("m-d-Y H:i:s", $datetimepicker2); ?></strong>. 
              <?php endif ?>
            </form>
              <button id="btnExport" onclick="fnExcelReport();" style="float: right;"> EXPORT </button>
            <br>
            <br>
            <table id="allDataExport" class="widefat fixed" cellspacing="0">
                <thead>
                    <tr>
                        <th id="cb" class="manage-column column-cb" scope="col">Person Details</th>
                        <th id="columnname" class="manage-column column-columnname" scope="col">Resource List</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th class="manage-column column-cb" scope="col">Person Details</th>
                        <th class="manage-column column-columnname" scope="col">Resource List</th>
                    </tr>
                </tfoot>

                <tbody>
                    <?php 
                        $i = 1;
                        foreach ($all_resources as $key => $resource_record) {
                            $classss = ($i % 2) ? 'alternate' : '';
                            echo '<tr class="'. $classss .'">';
                            $person_detail = json_decode($resource_record->person_detail);
                            echo '<td class="column-columnname">';
                            foreach ($person_detail as $key => $value) {
                                if ($key != '_empty_') {
                                    if ($key != 'Address') {
                                    if ($key != 'CAPTCHA') {
                                    echo '<strong>' . $key . ': </strong> ' . $value . '<br>';
                                    }
                                    }
                                }
                            }
                            echo '</td>';
                            echo '<td class="column-columnname">';
                            $viewss = explode(';', $resource_record->resources_views);
                            // echo "<ul style='list-style: inherit; list-style-position: inside;'>";
                            foreach ($viewss as $key => $value) {
                                if (!empty($value)) {
                                    echo '- ' . $value . '<br>';
                                }
                            }
                            // echo "</ul>";
                            if ($resource_record->date_time != '') {
                              echo '<strong>Date & Time: </strong>'. date("m-d-Y H:i:s", $resource_record->date_time);
                            }
                            echo '</td>';
                            echo '</tr>';
                            $i++;
                        }
                     ?>
                </tbody>
            </table>
            <?php
        }
        add_action( 'admin_menu', 'resources_detail_page' );

        function resource_cron_action() {
            global $wpdb;
            $get_resources = $wpdb->get_results( 'SELECT * FROM wp_resources WHERE is_inactive = 0', OBJECT );
            foreach ($get_resources as $key => $get_resource) {
                $all_detail = json_decode($get_resource->person_detail);
                $headers = 'From: ' . $all_detail->Name . ' <' . $get_resource->person_email . '>' . "\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $message = 'Person Detail: <br>';
                $message .= 'Name: ' . $all_detail->Name . '<br>';
                $message .= 'Phone: ' . $all_detail->Phone . '<br>';
                $message .= 'Company: ' . $all_detail->Company . '<br>';
                $message .= 'Email: ' . $get_resource->person_email . '<br>';
                $message .= '<br><br>Resource List: <br>';
                $all_resources = explode(';', $get_resource->resources_views);
                foreach ($all_resources as $key => $all_resource) {
                    if ($all_resource != '') {
                        $message .= $key . '. ' . $all_resource . '<br>';
                    }
                }
                $admemail = get_option( 'admin_email' );
                $admsubject = 'Resource Download Log for '.$all_detail->Name;
                wp_mail( $admemail, $admsubject, $message, $headers);
                $wpdb->update( 
                    'wp_resources', 
                    array( 
                        'is_inactive' => 1
                    ), 
                    array( 'id' => $get_resource->id ),
                    array( 
                        '%s'
                    ), 
                    array( '%s' ) 
                );
            }
        }
        add_action('resource_cron_action', 'resource_cron_action');

    // ADD CUSTOM POST TYPES
       function custom_post_type() {
          //TIMELINES
          $labels = array(
              'name'   => _x( 'Timelines', 'Post Type General Name', 'twentysixteen' ),
              'singular_nam'   => _x( 'Timeline', 'Post Type Singular Name', 'twentysixteen' ),
              'menu_name'    => __( 'Timelines', 'twentysixteen' ),
              'parent_item_colon'   => __( 'Parent Timeline', 'twentysixteen' ),
              'all_item'    => __( 'All Timelines', 'twentysixteen' ),
              'view_item'    => __( 'View Timeline', 'twentysixteen' ),
              'add_new_item'   => __( 'Add New Timeline', 'twentysixteen' ),
              'add_new'     => __( 'Add New', 'twentysixteen' ),
              'edit_item'    => __( 'Edit Timeline', 'twentysixteen' ),
              'update_item'      => __( 'Update Timeline', 'twentysixteen' ),
              'search_items'   => __( 'Search Timeline', 'twentysixteen' ),
              'not_found'    => __( 'Not Found', 'twentysixteen' ),
              'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
         );
         $args = array(
              'label'     => __( 'Timelines', 'twentysixteen' ),
              'description'      => __( 'Timeline events', 'twentysixteen' ),
              'labels'           => $labels,
              'supports'     => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
              'hierarchical'  => false,
              'public'           => true,
              'show_ui'     => true,
              'show_in_menu'   => true,
              'show_in_nav_menus'   => true,
              'show_in_admin_bar'   => true,
              'menu_position'   => 5,
              'can_export'         => true,
              'has_archive'      => true,
              'exclude_from_search' => false,
              'publicly_queryable'  => true,
              'capability_type'   => 'page',
         );
         register_post_type( 'Timelines', $args );
         //Locations
         $labels = array(
              'name'   => _x( 'Locations', 'Post Type General Name', 'twentysixteen' ),
              'singular_name'   => _x( 'Location', 'Post Type Singular Name', 'twentysixteen' ),
              'menu_name'    => __( 'Locations', 'twentysixteen' ),
              'parent_item_colon'   => __( 'Parent Location', 'twentysixteen' ),
              'all_item'    => __( 'All Locations', 'twentysixteen' ),
              'view_item'    => __( 'View Location', 'twentysixteen' ),
              'add_new_item'   => __( 'Add New Location', 'twentysixteen' ),
              'add_new'     => __( 'Add New', 'twentysixteen' ),
              'edit_item'    => __( 'Edit Location', 'twentysixteen' ),
              'update_item'      => __( 'Update Location', 'twentysixteen' ),
              'search_items'   => __( 'Search Location', 'twentysixteen' ),
              'not_found'    => __( 'Not Found', 'twentysixteen' ),
              'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
         );
         $args = array(
              'label'     => __( 'Locations', 'twentysixteen' ),
              'description'      => __( 'Location events', 'twentysixteen' ),
              'labels'           => $labels,
              'supports'     => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
              'hierarchical'   => false,
              'public'           => true,
              'show_ui'     => true,
              'show_in_menu'   => true,
              'show_in_nav_menus'   => true,
              'show_in_admin_bar'   => true,
              'menu_position'   => 5,
              'can_export'         => true,
              'has_archive'      => true,
              'exclude_from_search' => false,
              'publicly_queryable'  => true,
              'capability_type'   => 'page',
         );
         register_post_type( 'Locations', $args );
         //Affiliates
         $labels = array(
              'name'   => _x( 'Affiliates', 'Post Type General Name', 'twentysixteen' ),
              'singular_name'   => _x( 'Affiliate', 'Post Type Singular Name', 'twentysixteen' ),
              'menu_name'    => __( 'Affiliates', 'twentysixteen' ),
              'parent_item_colon'   => __( 'Parent Affiliate', 'twentysixteen' ),
              'all_item'    => __( 'All Affiliates', 'twentysixteen' ),
              'view_item'   => __( 'View Affiliate', 'twentysixteen' ),
              'add_new_item'   => __( 'Add New Affiliate', 'twentysixteen' ),
              'add_new'     => __( 'Add New', 'twentysixteen' ),
              'edit_item'    => __( 'Edit Affiliate', 'twentysixteen' ),
              'update_item'      => __( 'Update Affiliate', 'twentysixteen' ),
              'search_items'   => __( 'Search Affiliate', 'twentysixteen' ),
              'not_found'    => __( 'Not Found', 'twentysixteen' ),
              'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
         );
         $args = array(
              'label'    => __( 'Affiliates', 'twentysixteen' ),
              'description'      => __( 'Affiliate events', 'twentysixteen' ),
              'labels'           => $labels,
              'supports'     => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
              'hierarchical'   => false,
              'public'           => true,
              'show_ui'     => true,
              'show_in_menu'   => true,
              'show_in_nav_menus'   => true,
              'show_in_admin_bar'   => true,
              'menu_position'   => 5,
              'can_export'         => true,
              'has_archive'      => true,
              'exclude_from_search' => false,
              'publicly_queryable'  => true,
              'capability_type'   => 'page',
         );
         register_post_type( 'Affiliates', $args );
         //Jobs
         $labels = array(
              'name'   => _x( 'Jobs', 'Post Type General Name', 'twentysixteen' ),
              'singular_name'   => _x( 'Job', 'Post Type Singular Name', 'twentysixteen' ),
              'menu_name'    => __( 'Jobs', 'twentysixteen' ),
              'parent_item_colon'   => __( 'Parent Job', 'twentysixteen' ),
              'all_item'    => __( 'All Jobs', 'twentysixteen' ),
              'view_item'    => __( 'View Job', 'twentysixteen' ),
              'add_new_item'   => __( 'Add New Job', 'twentysixteen' ),
              'add_new'     => __( 'Add New', 'twentysixteen' ),
              'edit_item'    => __( 'Edit Job', 'twentysixteen' ),
              'update_item'      => __( 'Update Job', 'twentysixteen' ),
              'search_items'   => __( 'Search Job', 'twentysixteen' ),
              'not_found'    => __( 'Not Found', 'twentysixteen' ),
              'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
         );
         $args = array(
              'label'     => __( 'Jobs', 'twentysixteen' ),
              'description'      => __( 'Job events', 'twentysixteen' ),
              'labels'           => $labels,
              'supports'     => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
              'hierarchical'   => false,
              'public'           => true,
              'show_ui'     => true,
              'show_in_menu'   => true,
              'show_in_nav_menus'   => true,
              'show_in_admin_bar'   => true,
              'menu_position'   => 5,
              'can_export'         => true,
              'has_archive'      => true,
              'exclude_from_search' => false,
              'publicly_queryable'  => true,
              'capability_type'   => 'page',
         );
         register_post_type( 'Jobs', $args );
          //Team Members
         $labels = array(
              'name'   => _x( 'Members', 'Post Type General Name', 'twentysixteen' ),
              'singular_name'   => _x( 'Professional', 'Post Type Singular Name', 'twentysixteen' ),
              'menu_name'    => __( 'Members', 'twentysixteen' ),
              'parent_item_colon'   => __( 'Parent Member', 'twentysixteen' ),
              'all_item'    => __( 'All Members', 'twentysixteen' ),
              'view_item'    => __( 'View Member', 'twentysixteen' ),
              'add_new_item'   => __( 'Add New Member', 'twentysixteen' ),
              'add_new'     => __( 'Add New', 'twentysixteen' ),
              'edit_item'    => __( 'Edit Member', 'twentysixteen' ),
              'update_item'      => __( 'Update Member', 'twentysixteen' ),
              'search_items'   => __( 'Search Member', 'twentysixteen' ),
              'not_found'    => __( 'Not Found', 'twentysixteen' ),
              'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
         );
         $args = array(
              'label'     => __( 'Members', 'twentysixteen' ),
              'description'      => __( 'Member events', 'twentysixteen' ),
              'labels'           => $labels,
              'supports'     => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
              'taxonomies'         => array( 'category' ),
              'hierarchical'   => false,
              'public'           => true,
              'show_ui'     => true,
              'show_in_menu'   => true,
              'show_in_nav_menus'   => true,
              'show_in_admin_bar'   => true,
              'menu_position'   => 5,
              'can_export'         => true,
              'has_archive'      => true,
              'exclude_from_search' => false,
              'publicly_queryable'  => true,
              'capability_type'   => 'page',
         );
         register_post_type( 'Members', $args );
         //Press
         $labels = array(
              'name'   => _x( 'Press', 'Post Type General Name', 'twentysixteen' ),
              'singular_name'   => _x( 'Press', 'Post Type Singular Name', 'twentysixteen' ),
              'menu_name'    => __( 'Press', 'twentysixteen' ),
              'parent_item_colon'   => __( 'Parent Press', 'twentysixteen' ),
              'all_item'    => __( 'All Press', 'twentysixteen' ),
              'view_item'    => __( 'View Press', 'twentysixteen' ),
              'add_new_item'   => __( 'Add New Press', 'twentysixteen' ),
              'add_new'     => __( 'Add New', 'twentysixteen' ),
              'edit_item'    => __( 'Edit Press', 'twentysixteen' ),
              'update_item'      => __( 'Update Press', 'twentysixteen' ),
              'search_items'   => __( 'Search Press', 'twentysixteen' ),
              'not_found'    => __( 'Not Found', 'twentysixteen' ),
              'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
         );
         $args = array(
              'label'     => __( 'Press', 'twentysixteen' ),
              'description'      => __( 'Press events', 'twentysixteen' ),
              'labels'           => $labels,
              'supports'     => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
              'hierarchical'   => false,
              'public'           => true,
              'show_ui'     => true,
              'show_in_menu'   => true,
              'show_in_nav_menus'   => true,
              'show_in_admin_bar'   => true,
              'menu_position'   => 5,
              'can_export'         => true,
              'has_archive'      => true,
              'exclude_from_search' => false,
              'publicly_queryable'  => true,
              'capability_type'   => 'page',
         );
         register_post_type( 'Press', $args );
         //Resources
         $labels = array(
              'name'   => _x( 'Resources', 'Post Type General Name', 'twentysixteen' ),
              'singular_name'  => _x( 'Resource', 'Post Type Singular Name', 'twentysixteen' ),
              'menu_name'    => __( 'Resources', 'twentysixteen' ),
              'parent_item_colon'   => __( 'Parent Resource', 'twentysixteen' ),
              'all_item'    => __( 'All Resources', 'twentysixteen' ),
              'view_item'    => __( 'View Resource', 'twentysixteen' ),
              'add_new_item'   => __( 'Add New Resource', 'twentysixteen' ),
              'add_new'     => __( 'Add New', 'twentysixteen' ),
              'edit_item'    => __( 'Edit Resource', 'twentysixteen' ),
              'update_item'      => __( 'Update Resource', 'twentysixteen' ),
              'search_items'   => __( 'Search Resource', 'twentysixteen' ),
              'not_found'    => __( 'Not Found', 'twentysixteen' ),
              'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
         );
         $args = array(
              'label'     => __( 'Resources', 'twentysixteen' ),
              'description'      => __( 'Resource Resources', 'twentysixteen' ),
              'labels'           => $labels,
              'supports'     => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
              'taxonomies'         => array( 'category' ),
              'hierarchical'   => false,
              'public'           => true,
              'show_ui'     => true,
              'show_in_menu'   => true,
              'show_in_nav_menus'   => true,
              'show_in_admin_bar'   => true,
              'menu_position'   => 5,
              'can_export'         => true,
              'has_archive'      => true,
              'exclude_from_search' => false,
              'publicly_queryable'  => true,
              'capability_type'   => 'page',
         );
				 register_post_type( 'Resources', $args );
				 //Case Studies
				 $labels = array(
					'name' => __( 'Case Studies', 'Post Type General Name', 'textdomain' ),
					'singular_name' => __( 'Case Study', 'Post Type Singular Name', 'textdomain' ),
					'menu_name' => __( 'Case Studies', 'textdomain' ),
					'name_admin_bar' => __( 'Case Study', 'textdomain' ),
					'archives' => __( 'Case Study Archives', 'textdomain' ),
					'attributes' => __( 'Case Study Attributes', 'textdomain' ),
					'parent_item_colon' => __( 'Parent Case Study:', 'textdomain' ),
					'all_items' => __( 'All Case Studies', 'textdomain' ),
					'add_new_item' => __( 'Add New Case Study', 'textdomain' ),
					'add_new' => __( 'Add New', 'textdomain' ),
					'new_item' => __( 'New Case Study', 'textdomain' ),
					'edit_item' => __( 'Edit Case Study', 'textdomain' ),
					'update_item' => __( 'Update Case Study', 'textdomain' ),
					'view_item' => __( 'View Case Study', 'textdomain' ),
					'view_items' => __( 'View Case Studies', 'textdomain' ),
					'search_items' => __( 'Search Case Study', 'textdomain' ),
					'not_found' => __( 'Not found', 'textdomain' ),
					'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
					'featured_image' => __( 'Featured Image', 'textdomain' ),
					'set_featured_image' => __( 'Set featured image', 'textdomain' ),
					'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
					'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
					'insert_into_item' => __( 'Insert into Case Study', 'textdomain' ),
					'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'textdomain' ),
					'items_list' => __( 'Case Studies list', 'textdomain' ),
					'items_list_navigation' => __( 'Case Studies list navigation', 'textdomain' ),
					'filter_items_list' => __( 'Filter Case Studies list', 'textdomain' ),
				);
				$args = array(
					'label' => __( 'Case Study', 'textdomain' ),
					'description' => __( '', 'textdomain' ),
					'labels' => $labels,
					'menu_icon' => 'dashicons-media-text',
					'supports' => array('title', 'editor', 'excerpt', 'custom-fields', 'thumbnail', ),
					'taxonomies' => array('category'),
					'public' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					'menu_position' => 5,
					'show_in_admin_bar' => true,
					'show_in_nav_menus' => true,
					'can_export' => true,
					'has_archive' => true,
					'hierarchical' => false,
					'exclude_from_search' => false,
					'show_in_rest' => true,
					'publicly_queryable' => true,
					'capability_type' => 'post',
				);
				register_post_type( 'case-study', $args );
       }
       add_action( 'init', 'custom_post_type', 0 );
    // ADD WIDGET AREAS FOR CONTENT
       if ( ! function_exists( 'custom_sidebar' ) ) {
         function custom_sidebar() {
              //Single Right Sidebar
              $args = array(
                  'id'          => 'single-right-sidebar',
                  'name'    => __( 'Single Right Sidebar'),
                  'description'   => __( 'This is the sidebar found on the "Right Sidebar" template'),
                  'class'   => 'single-right-sidebar',
                  'before_title'  => '<h2 class="right-sidebar-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
              register_sidebar( $args );
              //Single Left Sidebar
              $args = array(
                  'id'          => 'single-left-sidebar',
                  'name'    => __( 'Single Left Sidebar'),
                  'description'   => __( 'This is the sidebar found on the "Left Sidebar" template'),
                  'class'   => 'single-left-sidebar',
                  'before_title'  => '<h2 class="left-sidebar-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
              register_sidebar( $args );
              //Two Sidebar Right
              $args = array(
                  'id'          => 'two-sidebar-right',
                  'name'   => __( 'Two Sidebar Right'),
                  'description'   => __( 'This is the sidebar found on the "Two Sidebar" template'),
                  'class'   => 'two-sidebar-right',
                  'before_title'  => '<h2 class="two-sidebar-right-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
              register_sidebar( $args );
              //Two Sidebar Left
              $args = array(
                  'id'          => 'two-sidebar-left',
                  'name'    => __( 'Two Sidebar Left'),
                  'description'   => __( 'This is the sidebar found on the "Two Sidebar" template'),
                  'class'   => 'two-sidebar-left',
                  'before_title'  => '<h2 class="two-sidebar-left-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
              register_sidebar( $args );
              //Viewpoint Archieve
              $args = array(
                  'id'          => 'archieve-sidebar',
                  'name'    => __( 'Archieve Sidebar'),
                  'description'   => __( 'This is the sidebar found on the Viewpoint archieves'),
                  'class'   => 'archieve-sidebar',
                  'before_title'  => '<h2 class="widget-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
              register_sidebar( $args );
              //Press Archieve
              $args = array(
                  'id'          => 'press-sidebar',
                  'name'    => __( 'Press Posts Sidebar'),
                  'description'   => __( 'This is the sidebar found on the Press archive and posts'),
                  'class'   => 'press-sidebar',
                  'before_title'  => '<h2 class="widget-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
              register_sidebar( $args );
              //Events Archieve
              $args = array(
                  'id'          => 'events-sidebar',
                  'name'    => __( 'Events Sidebar'),
                  'description'   => __( 'This is the sidebar found on the Events archiveand posts'),
                  'class'   => 'events-sidebar',
                  'before_title'  => '<h2 class="widget-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
              register_sidebar( $args );
              //Footer Awards
              $args = array(
                  'id'          => 'footer-sidebar',
                  'name'    => __( 'Footer Sidebar'),
                  'description'   => __( 'This is the sidebar in the footer to display 2 awards'),
                  'class'   => 'footer-sidebar',
                  'before_title'  => '<h2 class="widget-title">',
                  'after_title'   => '</h2>',
                  'before_widget' => '<div id="%1$s" class="widget %2$s">',
                  'after_widget'  => '</div>',
              );
							register_sidebar( $args );
							//Footer Legal
              $args = array(
								'id'          => 'footer-legal',
								'name'    => __( 'Footer Legal'),
								'description'   => __( 'This is the sidebar in the footer to display legal copy'),
								'class'   => 'footer-legal',
								'before_title'  => '<h2 class="widget-title">',
								'after_title'   => '</h2>',
								'before_widget' => '<div id="%1$s" class="widget %2$s">',
								'after_widget'  => '</div>',
						);
						register_sidebar( $args );
         }
       add_action( 'widgets_init', 'custom_sidebar' );
       }
//CUSTOMIZE CONTENT FOR CLIENT

function feature( $atts, $content = null) {
    extract(shortcode_atts(array(
        'color' => 'orange',
        'url' => '#'
    ), $atts));

    //create custom pager
    $doc = new DOMDocument;
    @$doc->loadHTML($content);
    $images = $doc->getElementsByTagName('img');

    foreach($images as $image) {
        $image_src = $image->getAttribute("src");
        $image_alt = $image->getAttribute("alt");
    }

    //remove image and empty p tags
    $content = preg_replace("/<img[^>]+\>/i", "", $content);
    $content = preg_replace('%<p>&nbsp;</p>%i', '$1', $content);
    $content = preg_replace('%<p></p>%i', '$1', $content);
    $content = preg_replace('#^<\/p>|<p>$#', '', $content);

    return '<div class="featured">
                <div class="featured-bg">
                    <div class="featured-image"><img src="'.$image_src.'" alt="'.$image_alt.'" width="100%"></div>
                    <div class="featured-content '.$color.'">'.do_shortcode($content).'</div>
                    <div class="featured-read-more"><a href="'.$url.'" style="padding: 5px 10px; background: #00558C; color: #fff; text-decoration: none;">Read More</a></div>
                </div>
            </div>';
}
add_shortcode("feature", "feature");
function content( $atts, $content = null) {
    extract(shortcode_atts(array(
        //"last" => ''
    ), $atts));

    //remove image and empty p tags
    $content = preg_replace('%<p>&nbsp;</p>%i', '$1', $content);
    $content = preg_replace('%<p></p>%i', '$1', $content);
    $content = preg_replace('#^<\/p>|<p>$#', '', $content);

    return '<div id="page-content-shortcode">'.do_shortcode($content).'</div>';
}
add_shortcode("content", "content");
function blog_posts( $atts, $content = null) {
    extract(shortcode_atts(array(
        "blog_category" => null,
        "blog_site" => 2,
        "posts_per_page" => 3,
        "title" => "Blog",
        "show_date" => 'true'
    ), $atts));

    if(get_current_blog_id() != $blog_site) switch_to_blog($blog_site);

    if(!empty($blog_category)) {
        $query = new WP_Query(array(
            'category__in' => $blog_category,
            'posts_per_page' => $posts_per_page
        ));
    } else {
        $query = new WP_Query(array(
            'posts_per_page' => $posts_per_page
        ));
    }

    if ( $query->have_posts() ) {

        $html = '';

        $html .= '<div class="box blog-box">
                <div class="box-heading">
                    <h2 style="background: #2d2d2d; color: #fff; padding: 5px; line-height: 30px;">'.$title.'</h2>
                </div>';

        $html .= '<div class="box-content">
                <ul>';

        while ( $query->have_posts() ) {
            $query->the_post();
            $html .= '<li>'.(($show_date == 'true') ? '<strong>'.get_the_date('m/d/y').'</strong>' : '' ).' <a href="'.get_permalink().'">'.get_the_title().'</a></li>';
        }

        $html .= '</ul></div></div>';
    } else {
        // no posts found, empty
    }

    wp_reset_postdata();
    restore_current_blog();

    return $html;
}
add_shortcode("blog_posts", "blog_posts");

//location column
function location( $atts, $content = null) {
    extract(shortcode_atts(array(
        "image" => '',
        "image_alt" => '',
        "image_url" => '#',
        "last" => ''
    ), $atts));

    return '<div class="newLocation location '.(($last==1) ? 'last' : '').'">
                <div class="image"><a href="'.$image_url.'"><img src="'.$image.'" alt="'.$image_alt.'"></a></div>
                <div class="description">'.do_shortcode($content).'</div>
            </div>';
}
add_shortcode("location", "location");
?>
<?php
if(get_current_blog_id() == 10 || get_current_blog_id() == 11) {
function directory_custom_init() {
    $labels = array(
        'name' => 'Employee',
        'singular_name' => 'Employee',
        'add_new' => 'Add Employee',
        'add_new_item' => 'Add New Employee',
        'edit_item' => 'Edit Employee',
        'new_item' => 'New Employee',
        'all_items' => 'All Employees',
        'view_item' => 'View Employee',
        'search_items' => 'Search Employee',
        'not_found' =>  'No Employees found',
        'not_found_in_trash' => 'No Employees found in Trash',
        'parent_item_colon' => '',
        'menu_name' => 'Employees'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'directory' ),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array( 'title', 'editor' )
    );

    register_post_type( 'directory', $args );

    //meta boxesro
    add_action('save_post','directory_info_save');
}
add_action( 'init', 'directory_custom_init' );

function directory_taxonomy() {
    register_taxonomy(
        'directory_location',
        'directory',
        array(
            'hierarchical' => true,
            'label' => (get_current_blog_id() != 11) ? 'Locations' : 'Title',
            'query_var' => true,
            'rewrite' => array('slug' => 'directory-location'),
        )
    );
}
add_action( 'init', 'directory_taxonomy' );



add_action( 'admin_init', 'directory_info_metabox' );
function directory_info_metabox() {
    add_meta_box('directory_info', 'Employee Information', 'directory_info_box', 'directory', 'normal', 'high');
}

add_action( 'save_post', 'directory_info_save' );


function directory_info_box( $post ) {
    $photo = get_post_meta($post->ID,'_employee_photo',TRUE);

    wp_nonce_field( plugin_basename( __FILE__ ), 'directory_info_box_content_nonce' );
    echo '
        <script type="text/javascript">
            jQuery(function() {
                var _ste;
                var input;
                var callback = function( image_url ){
                    var path;

                    if(jQuery(jQuery(image_url)).find("img").size() > 0) {
                        path = jQuery("img",image_url).attr("src");
                    }else{
                        path = jQuery(image_url).attr("href");
                    }

                    input.val(path);

                    send_to_editor = _ste;
                    tb_remove();
                };

                jQuery(".meta-add-media").click(function(){
                      if( typeof(send_to_editor) == "function" ) {
                            input = jQuery(this).prev("input");
                            _ste = send_to_editor;
                            send_to_editor = callback
                      }
                });
            });
        </script>

        <p>
            <label for="_employee_title">Title</label><br>
            <input type="text" id="_employee_title" name="_employee_title" value="'.get_post_meta($post->ID,'_employee_title',TRUE).'" size="60">
        </p>

        <p>
            <label for="_employee_phone">Phone</label><br>
            <input type="text" id="_employee_phone" name="_employee_phone" value="'.get_post_meta($post->ID,'_employee_phone',TRUE).'" size="60">
        </p>

        <p>
            <label for="_employee_email">Email</label><br>
            <input type="text" id="_employee_email" name="_employee_email" value="'.get_post_meta($post->ID,'_employee_email',TRUE).'" size="60">
        </p>

        <p>
            <label for="_employee_specialty">Specialty</label><br>
            <input type="text" id="_employee_specialty" name="_employee_specialty" value="'.get_post_meta($post->ID,'_employee_specialty',TRUE).'" size="60">
        </p>

        <p>
            <label for="_employee_credentials">Credentials</label><br>
            <input type="text" id="_employee_credentials" name="_employee_credentials" value="'.get_post_meta($post->ID,'_employee_credentials',TRUE).'" size="60">
        </p>

        <p>
            <label for="_employee_profile">Profile</label><br>
            <input id="_employee_profile" type="text" size="40" name="_employee_profile" value="'.get_post_meta($post->ID,'_employee_profile',TRUE).'">
            <input type="button" value="Upload File" class="button insert-media meta-add-media add_media">
        </p>

        <p>
            <label for="_employee_photo">Photo</label><br>
            <input id="_employee_photo" type="text" size="40" name="_employee_photo" value="'.get_post_meta($post->ID,'_employee_photo',TRUE).'">
            <input type="button" value="Upload File" class="button insert-media meta-add-media add_media">
        </p>

        '.((!empty($photo)) ? '<p><img src="'.$photo.'"></p>' : '');
}

function directory_info_save( $post_id ) {

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    if ( !wp_verify_nonce( $_POST['directory_info_box_content_nonce'], plugin_basename( __FILE__ ) ) )
        return;

    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) )
            return;
    } else {
        if ( !current_user_can( 'edit_post', $post_id ) )
            return;
    }

    $full_name = explode(" ",$_POST['post_title']);
    $last_name = end($full_name);

    update_post_meta( $post_id, '_employee_last_name', $last_name );
    update_post_meta( $post_id, '_employee_title', $_POST['_employee_title'] );
    update_post_meta( $post_id, '_employee_phone', $_POST['_employee_phone'] );
    update_post_meta( $post_id, '_employee_email', $_POST['_employee_email'] );
    update_post_meta( $post_id, '_employee_credentials', $_POST['_employee_credentials'] );
    update_post_meta( $post_id, '_employee_specialty', $_POST['_employee_specialty'] );
    update_post_meta( $post_id, '_employee_photo', $_POST['_employee_photo'] );
    update_post_meta( $post_id, '_employee_profile', $_POST['_employee_profile'] );

}
}

//ACF GOOGLE MAPS

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDEBSbKNbfVVS_9_l7zOo5MBu7XBYc0oL0');
}

add_action('acf/init', 'my_acf_init');

// Register Custom Post Type Compliance Support
function create_compliancesupport_cpt() {

	$labels = array(
		'name' => _x( 'Compliance Support', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Compliance Support', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Compliance Support', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Compliance Support', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Compliance Support Archives', 'textdomain' ),
		'attributes' => __( 'Compliance Support Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Compliance Support:', 'textdomain' ),
		'all_items' => __( 'All Compliance Support', 'textdomain' ),
		'add_new_item' => __( 'Add New Compliance Support', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Compliance Support', 'textdomain' ),
		'edit_item' => __( 'Edit Compliance Support', 'textdomain' ),
		'update_item' => __( 'Update Compliance Support', 'textdomain' ),
		'view_item' => __( 'View Compliance Support', 'textdomain' ),
		'view_items' => __( 'View Compliance Support', 'textdomain' ),
		'search_items' => __( 'Search Compliance Support', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Compliance Support', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Compliance Support', 'textdomain' ),
		'items_list' => __( 'Compliance Support list', 'textdomain' ),
		'items_list_navigation' => __( 'Compliance Support list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Compliance Support list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Compliance Support', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-editor-help',
		'supports' => array('title', 'thumbnail', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'compliance-support', $args );

}
add_action( 'init', 'create_compliancesupport_cpt', 0 );

/*----------------------------------------------------------------*\
	LOGIN REDIRECT FOR USERS OTHER THAN ADMINS
\*----------------------------------------------------------------*/
function my_login_redirect( $url, $request, $user ){
	if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
		if( $user->has_cap( 'administrator')) {
			$url = admin_url();
		} else {
			$url = home_url('/compliance-support/');
		}
	}
	return $url;
}
add_filter('login_redirect', 'my_login_redirect', 10, 3 );

/*----------------------------------------------------------------*\
	OPTION PAGES
\*----------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Compliance Info',
		'menu_title'	=> 'Compliance Info',
		'menu_slug' 	=> 'compliance-info',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

add_theme_support('post-thumbnails', array(
	'post',
	'page',
	'members',
	'events',
	'locations',
	'resources',
));

/*----------------------------------------------------------------*\
	HIDE WORDPRESS ADMIN BAR FROM SUBSCRIBERS
\*----------------------------------------------------------------*/
// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_false');
}