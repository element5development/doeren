<?php /*
TEMPLATE FOR DISPLAYING NEWSLETTER SIGNUP SECTION
*/ ?>

<div id="newsletter-container" class="full-width">
	<div class="max-width" style="max-width: 800px;">
		<h1>Stay in the Know</h1>
		<p style="margin: 0 0 0 2em; position: initial;">Subscribe to our communications</p>
		<a href="https://doeren.com/subscribe/" class="primary-button" style="float: right; margin-top: 4px;">Subscribe</a>	
		<div style="clear: both"></div>
	</div>
</div>