<?php /*
TEMPALTE FOR DISPLAYING CUSTOM POST TYPE "AFFILIATES"
*/ ?>

<div class="affiliations-feed feed-cotainer">
	<?php
		query_posts( array( 'post_type' => 'affiliates', 'order' => 'ASC', 'posts_per_page' => -1 ) );
		if ( have_posts() ) : while ( have_posts() ) : the_post();
	?>

		<div class="affiliate full-width">
			<div class="picture one-third">
				<div class="featured"><?php the_post_thumbnail(); ?></div>
			</div>
			<div class="contents two-third">
				<div class="vertical-center">
					<h3><?php the_title(); ?></h3>
					<?php the_content(); ?>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>

	<?php endwhile; endif; wp_reset_query(); ?>
	<div style="clear: both"></div>
</div>

