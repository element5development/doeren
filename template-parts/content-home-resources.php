<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "RESOURCES" ON THE HOMEPAGE
*/ ?>

<?php
	$cookie_name = "resource-download";
?>

<?php $args = array(
	'meta_key'    => '_thumbnail_id',
    'posts_per_page' => 20,
    'offset' => 0,
    'post_type' => 'resources'
);

$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>
		<?php
			if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
				<a class="home-resource pop-up-form">
			<?php } else { ?>
				<a class="home-resource" href="<?php the_permalink(); ?>">
			<?php }
		?>

			<article id="post-<?php the_ID(); ?>" class="post-feed full-width <?php echo (++$j % 2 == 0) ? 'even' : 'odd'; ?>">
				<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>
				<div class="post-feed-image" style="background-image: url(<?php echo $src[0]; ?> )">
					<div class="post-gradient"></div>
				</div>
				<div class="resource-contents">
					<div class="resource-format"><?php the_field( 'format' ) ?></div>
					<?php
						if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
							<p class="entry-header pop-up-form">
								<?php 
									$title  = the_title('','',false);
									if(strlen($title) > 100) {
									    echo trim(substr($title, 0, 100)).'...';
									} else {
									    echo $title;
									}
								?>
							</p>
						<?php } else { ?>
								<p class="entry-header">
									<?php 
										$title  = the_title('','',false);
										if(strlen($title) > 100) {
										    echo trim(substr($title, 0, 100)).'...';
										} else {
										    echo $title;
										}
									?>
								</p>
						<?php }
					?>
				</div>
				<div style="clear: both"></div>
			</article>
		</a>
	<?php }
} ?>
