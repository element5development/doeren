<?php /*
TEMPLATE FOR DISPLAYING PAGE HEADERS (IMAGE, TITLE, DESCRIPTION)
*/ ?>

<?php if ( get_field( 'page_header' ) ) { ?>
	<div class="page-header max-width" style="background-image: url(<?php the_field( 'page_header' ) ?>); <?php if(get_current_blog_id() == 10 || get_current_blog_id() == 11 && is_front_page()) { echo 'display: none';} ?>">
<?php } else { ?>
	<div class="page-header max-width" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/img/deafult-page-header.jpg'); <?php if(get_current_blog_id() == 10 || get_current_blog_id() == 11 && is_front_page()) { echo 'display: none';} ?>">
<?php } ?>

	<?php if( get_field('header_slider') ): ?>
		<?php the_field( 'header_slider' ) ?>
	<?php elseif( get_field('header-logo') ): ?>
		<div class="page-header-contents">
			<img src="<?php the_field( 'header-logo' ) ?>" />
		</div>
	<?php else: ?>
		<div class="page-header-contents">
			<div class="header-center">
				<h1 class="page-title"><?php echo get_the_title( $ID ); ?></h1>
				<p class="page-desctiption"><?php the_field( 'page_description' ) ?></p>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php if(get_current_blog_id() == 10 && (is_page('history') || is_page('locations'))) { echo '<style>#about-nav {display: none;}</style>';} ?>
