<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "RESOURCES" WITH ISSUE FORMAT 
*/ ?>

<?php
	$cookie_name = "resource-download";
?>

<article id="post-<?php the_ID(); ?>" class="post-feed resource grey-bg">

	<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>
	<div class="post-feed-image" style="background-image: url(<?php echo $src[0]; ?> )">
	</div>

	<div class="resource-contents">
		<?php
			if(!isset($_COOKIE[$cookie_name])) { ?>
				<h3 class="entry-header pop-up-form">
					<?php the_title(); ?>
				</h3>
			<?php } else { ?>
				<a href="<?php the_permalink(); ?>">
					<h3 class="entry-header">
						<?php the_title(); ?>
					</h3>
				</a>
			<?php }
		?>
		<?php
			if(!isset($_COOKIE[$cookie_name])) { ?>
			   	<a class="pop-up-form read-more">Sign in to read <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a>
			<?php } else { ?>
				<a class="read-more" href="<?php the_permalink(); ?>">Read this issue <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 
			<?php }
		?>
	</div>
	<div style="clear: both"></div>

</article>
