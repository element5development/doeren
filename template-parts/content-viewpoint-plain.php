<?php /*
TEMPLATE FOR DISPLAYING POSTS (VIEWPOINT) WITHOUT A FEATURED IMAGE
*/ ?>


	<article id="post-<?php the_ID(); ?>" class="post-feed viewpoint-no-image">

		<a href="<?php the_permalink(); ?>">
			<h3 class="entry-header">
				<?php echo substr(the_title($before = '', $after = '', FALSE), 0, 55); ?>...
			</h3>
		</a>
		<div class="post-feed-date"><?php the_time('m.d.y') ?></div> 

	</article>
