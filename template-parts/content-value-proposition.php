<?php /*
TEMPLATE FOR DISPLAYING CUSTOM CONTENT FOUND ON VALUE PROPOSITION PAGE
*/ ?>

<div class="full-width proposition-blue">
	<div class="one-fourth">
		<h2><?php the_field( 'blue_title' ) ?></h2>
	</div>
	<div class="three-fourth">
		<p><?php the_field( 'blue_content' ) ?></p>
	</div>
	<div style="clear: both"></div>
</div>
<div class="full-width proposition-grey">
	<div class="one-fourth">
		<h2><?php the_field( 'grey_title' ) ?></h2>
	</div>
	<div class="three-fourth">
		<p><?php the_field( 'grey_content' ) ?></p>
	</div>
	<div style="clear: both"></div>	
</div>
<div class="full-width proposition-green">
	<div class="one-fourth">
		<h2><?php the_field( 'green_title' ) ?></h2>
	</div>
	<div class="three-fourth">
		<p><?php the_field( 'green_content' ) ?></p>
	</div>
	<div style="clear: both"></div>
</div>

