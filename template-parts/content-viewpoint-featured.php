<?php /*
TEMPLATE FOR DISPLAYING POST SET AS FEATURED
*/ ?>

<?php
    $args = array(
		'posts_per_page' => 1,
		'meta_key'		 => 'featured_post',
		'meta_value'	 => true
	);

	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
?>

			<article id="post-<?php the_ID(); ?>" class="post-feed">
				<div class="one-half">
					<div class="post-feed-date">
						<span class="month"><?php the_time('M') ?></span>	
						<span class="day"><?php the_time('j') ?></span>
						<span class="year"><?php the_time('Y') ?></span>
					</div>
					<?php 
						//USE FEATURED IAMGE OTHERWISE USE DEFAULT IAMGE
						$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); 
						if ( $src[0] == "") {
							$featuredimg = "/wp-content/themes/doeren-mayhew/img/default-blog-image.png";
						} else {
							$featuredimg = $src[0];
						}	
					?>
					<div class="post-feed-image" style="background-image: url(<?php echo $featuredimg; ?> )">
					</div> 
				</div>
				<div class="one-half featured-post-contents">
					<a href="<?php the_permalink(); ?>"><h3 class="entry-header"><?php the_title(); ?></h3></a>
					<div class="post-feed-excerpt">
						<?php if ( get_field('description') ) {
							the_field('description');
						} else {
							$content = get_the_excerpt(); 
							echo substr($content, 0, 250);
						} ?>	
					</div> 
					<a class="read-more" href="<?php the_permalink(); ?>">Read Full Article <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 
				</div>
				<div style="clear: both"></div>
			</article>
<?php
		}
	}
?>
