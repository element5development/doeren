<?php /*
TEMPLATE USED FOR DISPLAYING CUSTOM POST TYPE "MEMBERS" AND FILTERING CAPABILITIES
*/ ?>

<?php
	$location = $_GET["location"];
	$industry = $_GET["industry"];
	$specialization = $_GET["specialization"];
	
	if ( $location == 'none' ) { unset($location); }
	if ( $industry == 'none' ) { unset($industry); }
	if ( $specialization == 'none' ) { unset($specialization); }


	if ( $location == NULL and $specialization == NULL and $industry == NULL) {
		/*NO VARIABLES PASSED*/
		$args = [ 
			'post_type' => 'members', 
			'order' => 'ASC', 
			'orderby' => 'title',
			'posts_per_page' => -1,
	    ];
	} elseif ( $specialization == NULL and $location == NULL ) {
	    /*NO SPECILIZATION OR LOCATION*/
		$args = [ 
			'post_type' => 'members', 
			'order' => 'ASC',
			'orderby' => 'title', 
			'posts_per_page' => -1,
			'meta_query' => array(
		        array(
		            'key' => 'member_industry',
		            'value' => $industry, 
		            'compare' => 'LIKE'
		        )
		    )
	    ];
	} elseif ( $industry == NULL and $location == NULL ) {
	    /*NO INDUSTRY OR LOCATION*/
		$args = [ 
			'post_type' => 'members', 
			'order' => 'ASC', 
			'orderby' => 'title',
			'posts_per_page' => -1,
			'meta_query' => array(
		        array(
		            'key' => 'member_expertise',
		            'value' => $specialization, 
		            'compare' => 'LIKE'
		        )
		    )
	    ];
	} elseif ( $industry == NULL and $specialization == NULL ) {
	    /*NO INDUSTRY OR SPECILIZATION*/
		$args = [ 
			'post_type' => 'members', 
			'order' => 'ASC', 
			'orderby' => 'title',
			'posts_per_page' => -1,
			'meta_query' => array(
		        array(
		            'key' => 'member_locations',
		            'value' => $location, 
		            'compare' => 'LIKE'
		        )
		    )
	    ];
	} elseif ( $industry == NULL ) {
	    /*NO INDUSTRY*/
		$args = [ 
			'post_type' => 'members',
			'order' => 'ASC', 
			'orderby' => 'title',
			'posts_per_page' => -1,
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key' => 'member_locations',
		            'value' => $location, 
		            'compare' => 'LIKE'
				),
				array(
					'key' => 'member_expertise',
		            'value' => $specialization, 
		            'compare' => 'LIKE'
				),
			),
	    ];
	} elseif ( $specialization == NULL ) {
		/*NO SPECIALIZATION*/
		$args = [ 
			'post_type' => 'members', 
			'order' => 'ASC', 
			'orderby' => 'title',
			'posts_per_page' => -1,
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key' => 'member_locations',
		            'value' => $location, 
		            'compare' => 'LIKE'
				),
				array(
					'key' => 'member_industry',
		            'value' => $industry, 
		            'compare' => 'LIKE'
				),
			),
	    ];
	} elseif ( $location == NULL ) {
		/*NO LOCATION*/
		$args = [ 
			'post_type' => 'members', 
			'order' => 'ASC', 
			'orderby' => 'title',
			'posts_per_page' => -1,
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key' => 'member_industry',
		            'value' => $industry, 
		            'compare' => 'LIKE'
				),
				array(
					'key' => 'member_expertise',
		            'value' => $specialization, 
		            'compare' => 'LIKE'
				),
			),
	    ];
	} else {
		/*EVERYTHING PASSED*/
		$args = [ 
			'post_type' => 'members', 
			'order' => 'ASC',
			'orderby' => 'title', 
			'posts_per_page' => -1,
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key' => 'member_industry',
		            'value' => $industry, 
		            'compare' => 'LIKE'
				),
				array(
					'key' => 'member_expertise',
		            'value' => $specialization, 
		            'compare' => 'LIKE'
				),
				array(
					'key' => 'member_locations',
		            'value' => $location, 
		            'compare' => 'LIKE'
				),
			),
	    ];
	}
?>

<div class="filter-search">
	<div class="two-third">
		<form id="category-filter" method="get" action="/about-doeren_mayhew/directory/">
		    <h2>Filter By:</h2>
		    <select name="location">
		    	<option value="none">Location</option>
		    	<option value="Charlotte, NC">Charlotte, NC</option>
		    	<option value="Dallas, TX">Dallas, TX</option>
		    	<option value="Houston, TX">Houston, TX</option>
		    	<option value="Miami, FL">Miami, FL</option>
		    	<option value="Troy, MI">Troy, MI</option>
		    </select>
		    <select name="industry">
		    	<option value="none">Industry</option>
		    	<option value="Automotive Dealerships">Automotive Dealerships</option>
		    	<option value="Closely Held Businesses">Closely Held Businesses</option>
		    	<option value="Construction">Construction</option>
		    	<option value="Dental CPA">Dental CPA</option>
		    	<option value="Energy">Energy</option>
		    	<option value="Financial Institutions">Financial Institutions</option>
		    	<option value="Governmental and Non-Profit">Governmental and Non-Profit</option>
		    	<option value="Health Care">Health Care</option>
					<option value="Manufacturing">Manufacturing</option>
		    	<option value="Real Estate">Real Estate</option>
		    	<option value="Retail and Restaurant">Retail and Restaurant</option>
		    	<option value="Service Providers">Service Providers</option>
		    	<option value="Technologies">Technologies</option>
		    	<option value="Wholesale and Distribution">Wholesale and Distribution</option>
		    </select>
		    <select name="specialization">
		    	<option value="none">Expertise</option>
		    	<option value="Accounting Audit and Assurance">Accounting Audit and Assurance</option>
		    	<option value="Business advisory">Business advisory</option>
		    	<option value="Dental CPA">Dental CPA</option>
		    	<option value="Employee benefit plans">Employee Benefit Plans</option>
		    	<option value="Insurance">Insurance</option>
		    	<option value="International Business">International Business</option>
		    	<option value="IT assurance and Security">IT Assurance and Security</option>
		    	<option value="Lending Reviews">Lending Reviews</option>
		    	<option value="Mergers and acquisitions">Mergers and Acquisitions</option>
		    	<option value="Payroll">Payroll</option>
		    	<option value="Practice Development">Practice Development</option>
		    	<option value="Regulatory Compliance">Regulatory Compliance</option>
		    	<option value="Strategic advisory">Strategic Advisory</option>
		    	<option value="Tax compliance and Planning">Tax Compliance and Planning</option>
		    	<option value="Valuation and Litigation Support">Valuation and Litigation Support</option>
		    	<option value="Wealth Management">Wealth Management</option>
		    </select>
		    <button type="submit" value="Submit">Apply</button>
	 	</form>
	</div>
	<div class="one-third">
		<form role="search" method="get" id="searchform" class="searchform" action="/"> 
			<h2>Search Directory:</h2>
			<input type="text" value="" name="s" placeholder="Search..." /> 
			<input type="hidden" name="post_type" value="members" />
			<button type="submit" value="Search">Search</button>
		</form>
	</div>
	<div style="clear: both"></div>
</div>

<div class="team-member-feed feed-cotainer">
	<?php
		query_posts( $args );
		if ( have_posts() ) : while ( have_posts() ) : the_post();
	?>
		<?php if( get_field('add_to_directory') ) { ?>
				<div class="team-member one-third">
					<div class="picture one-third">
						<div class="featured"><?php the_post_thumbnail(); ?></div>
					</div>
					<div class="contents two-third">
						<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
						<span>Location:</span><?php the_field( 'member_locations' ) ?><br/>
						<div class="contacts">
							<a class="phone-icon" href="tel:<?php the_field( 'phone_number' ) ?>">
								<div class="hover-text"><?php the_field( 'phone_number' ) ?></div>
							</a>
							<a class="email-icon" href="mailto:<?php the_field( 'email' ) ?>">
								<div class="hover-text"><?php the_field( 'email' ) ?></div>
							</a>
							<a class="vcard-icon" href="<?php the_field( 'vcard' ) ?>"></a>
						</div>
					</div>
					<div style="clear: both"></div>
				</div>
			<?php } ?>
		<?php endwhile; ?> 
		<?php else : ?>
		<article>
			<h2>No Professionals Were Found</h2>
		</article>
		<hr>
		<h2></h2>
		<?php 
			$argss = [ 
				'post_type' => 'members', 
				'order' => 'ASC', 
				'posts_per_page' => -1,
		    ];
			query_posts( $argss );
			if ( have_posts() ) : while ( have_posts() ) : the_post();
		?>
				<div class="team-member one-third">
					<div class="picture one-third">
						<div class="featured"><?php the_post_thumbnail(); ?></div>
					</div>
					<div class="contents two-third">
						<a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
						<span>Location:</span><?php the_field( 'member_locations' ) ?><br/>
						<div class="contacts">
							<a class="phone-icon" href="tel:<?php the_field( 'phone_number' ) ?>">
								<div class="hover-text">Phone Number</div>
							</a>
							<a class="email-icon" href="mailto:<?php the_field( 'email' ) ?>">
								<div class="hover-text">Email Address</div>
							</a>
							<a class="vcard-icon" href="<?php the_field( 'vcard' ) ?>">
								<div class="hover-text">Vcard Download</div>
							</a>
						</div>
					</div>
					<div style="clear: both"></div>
				</div>
			<?php endwhile; ?><?php endif; ?>
	<?php endif; ?>
	<div style="clear: both"></div>
</div>

