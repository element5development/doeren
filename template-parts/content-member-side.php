<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "MEMBERS" WHEN FOUND IN SIDEBAR
*/ ?>

<li id="post-<?php the_ID(); ?>" class="member-side">
	<a href="<?php echo get_permalink(); ?>"><div class="member-image"><?php the_post_thumbnail(); ?></div></a>
	<div class="member-details">
		<p class="name"><?php the_title(); ?></p>
		<p class="location"><?php the_field( 'member_locations' ) ?></p>
		<a class="secondary-button" href="tel:+<?php the_field( 'phone_number' ) ?>"><?php the_field( 'phone_number' ) ?></a>
		<a class="secondary-button" href="mailto:<?php the_field( 'email' ) ?>"><?php the_field( 'email' ) ?></a>
		<a class="secondary-button" href="<?php echo get_permalink(); ?>">Learn More</a>
	</div>
</li>
