<?php /*
TEMPLATE FOR DISPLAYING POSTS (VIEWPOINT) ON THE HOMEPAGE
*/ ?>

<?php $args = array(
	'posts_per_page' => 20,
	'offset' => 0,
);

$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>
		<a class="home-viewpoint" href="<?php the_permalink(); ?>">
			<article id="post-<?php the_ID(); ?>" class="post-feed full-width <?php echo (++$j % 2 == 0) ? 'even' : 'odd'; ?>">

				<?php 
					//USE FEATURED IAMGE OTHERWISE USE DEFAULT IAMGE
					$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); 
					if ( $src[0] == "") {
						$featuredimg = "/wp-content/themes/doeren-mayhew/img/default-blog-image.png";
					} else {
						$featuredimg = $src[0];
					}	
				?>
				<div class="post-feed-image" style="background-image: url(<?php echo $featuredimg; ?> )">
					<div class="post-gradient"></div>
				</div>
				<div class="viewpoint-image-contents">
						<p class="entry-header">
							<?php 
								$title  = the_title('','',false);
								if(strlen($title) > 100) {
								    echo trim(substr($title, 0, 100)).'...';
								} else {
								    echo $title;
								}
							?>
						</p>
					<div class="post-feed-date"><?php the_time('m.d.Y') ?></div>
				</div>
				<div style="clear: both"></div>

			</article>
		</a>
	<?php }
} ?>
