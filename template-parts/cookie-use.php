<?php 
/*----------------------------------------------------------------*\

	COOKIE USE POP UP

\*----------------------------------------------------------------*/
?>

<section class="cookie-use">
	<div class="block">
		<p>We use cookies to improve your experience and optimize user-friendliness. Read our <a href="/privacy-policy/">privacy policy</a> for more information on the cookies we use and how to delete or block them. To continue browsing our site, please click accept.</p>
		<button class="primary-button">Accept and Close</button>
	</div>
</section>