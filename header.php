<?php /*
THE TEMPLATE FOR DISPLAYING UNIVERSAL HEADER
*/ ?>

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" />
<!-- Mobile Site Media Query -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Main CSS -->
	<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
<!-- Databreach Calculator CSS -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/data_breach_calculator.css" />
<!-- Entrance Animations -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/animate.css" />
<!-- PRINT STYLES -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/print.css" />	
<!-- Load More Posts -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/load-more.js"></script>
<!-- Load More Posts -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/isotope.pkgd.js"></script>
<!-- Carousel Lead Contacts -->
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.flipster.css" rel="stylesheet" type="text/css">
<!-- Add to Calendar Button -->
	<link href="https://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
<!-- Google Maps API
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEBSbKNbfVVS_9_l7zOo5MBu7XBYc0oL0"></script> -->

<!-- Gravity Form Validation Styleing -->
	<style>
		.pop-up form .validation_error { display: none !important; }
		.gform_wrapper li.gfield.gfield_error.gfield_contains_required div.gfield_description { padding: 0 !important; }
		.gform_wrapper li.gfield.gfield_error.gfield_contains_required {			
			display: inline-block;
			margin: 0 0 8px 0 !important;
			vertical-align: top;
			width: 50%;
		}
		.gform_wrapper li.gfield.gfield_error.gfield_contains_required div.ginput_container, 
		.gform_wrapper li.gfield.gfield_error.gfield_contains_required label.gfield_label {
			margin-top: 0 !important;
		}
	</style>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N4BTM7N');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?> onload="_googWcmGet('callback', '1-888-870-9873')">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N4BTM7N"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Add to Calendar Button -->
	<script type="text/javascript">(function () {
		if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
		if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
				var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
				s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
				s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
				var h = d[g]('body')[0];h.appendChild(s); }})();
	</script>

	<!-- Cookie Banner -->
	<?php get_template_part('template-parts/cookie-use'); ?>

	<header id="mobile-nav" class="full-width">
		<div class="full-width"> 
			<a href="tel:+1-888-870-9873"><div id="call-icon"></div></a>
			<a href="<?php echo get_home_url(); ?>"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/doeren-mayhew-logo.png" alt="Doeren Mayhew" /></a>
			<div id="menu-icon"></div>
			<div style="clear: both"></div>
		</div>
		<div id="mobile-nav-expand" class="full-width">
			<a href="/contact/"><div class="message mobile-button">Message</div></a>
			<a href="/about-doeren_mayhew/locations/"><div class="locations mobile-button">Locations</div></a>
			<a href="tel:+1-888-870-9873"><div class="call mobile-button">Call</div></a>
			<div style="clear: both"></div>
			<nav>
				<?php switch_to_blog(1); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'mobile-nav' ) ); ?>
				<?php restore_current_blog(); ?>
			</nav> 
			<div style="clear: both"></div>
		</div>
	</header>
	<header id="main-header" class="full-width">
		<nav id="top-bar">
			<div class="one-half blue">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-blue.svg" />
				<a href="tel:+18888709873" id="number_link">+1 (888) 870-9873</a>
			</div>
			<div class="one-half grey">
				<nav>
					<?php switch_to_blog(1); ?>
						<?php wp_nav_menu( array( 'theme_location' => 'top-nav' ) ); ?>
					<?php restore_current_blog(); ?>
				</nav> 
				<form role="search" method="get" class="search-form" action="/">
					<label>
						<span class="screen-reader-text">Search for:</span>
						<input type="search" class="search-field" placeholder="Search�" name="s">
						<input type="hidden" name="searchblogs" value="9,8,7,6,5,4,3,2,1" />
					</label>
					<button type="submit" class="search-submit"><span class="screen-reader-text">Search</span></button>
				</form>
				<div style="clear: both"></div>
			</div>
			<div style="clear: both"></div>
		</nav>
		<div id="main-navigation" class="full-width">
			<div class="max-width">
				<a href="<?php echo get_home_url(); ?>"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/doeren-mayhew-logo.png" alt="Doeren Mayhew" /></a>
				<nav id="main-nav">
					<?php switch_to_blog(1); ?>
						<?php wp_nav_menu( array( 'theme_location' => 'main-nav' ) ); ?>
					<?php restore_current_blog(); ?>
				</nav> 
				<div style="clear: both"></div>
			</div>
		</div>
	</header>
	<div id="services-sub-menu" class="mega-menu max-width">
		<?php switch_to_blog(1); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'services-nav' ) ); ?>
		<?php restore_current_blog(); ?>
		<div style="clear: both"></div>
	</div>
	<div id="specializations-sub-menu" class="mega-menu max-width">
		<?php switch_to_blog(1); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'specialization-nav' ) ); ?>
		<?php restore_current_blog(); ?>
		<div style="clear: both"></div>
	</div>
	<div id="resources-sub-menu" class="mega-menu max-width">
		<div class="featured">
			<h2>Featured Resource</h2>
			<hr>
			<div id="viewpoint-featured">
			<?php switch_to_blog(1); ?>
				    <?php $args = array(
				    	'meta_key'    => '_thumbnail_id',
					    'posts_per_page' => 1,
					    'offset' => 0,
					    'post_type' => 'resources',
							'meta_query' => array(
								array(
									'key' => 'featured_resource',
									'value' => '1',
									'compare' => '=='
								)
							)
					);

					$the_query = new WP_Query( $args );
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'resource-featured-menu' );
						}
					} ?>
					<?php wp_reset_query(); ?>
				<?php restore_current_blog(); ?>
			</div>
			<!--<h2>Tax Reform</h2>
			<hr>
			<div id="viewpoint-featured">
			<?php $the_query = new WP_Query( 'page_id=12397' ); ?>
			<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
        <?php get_template_part( 'template-parts/content', 'featured-page' ); ?>
    	<?php endwhile;?>
			</div>-->
		</div>
		<div class="recent">
			<h2>Popular Resources</h2>
			<hr>
			<?php switch_to_blog(1); ?>
				<?php $args = array(
				    'posts_per_page' => 3,
				    'offset' => 1,
				    'post_type' => 'resources',
						'meta_query' => array(
							array(
								'key' => 'featured_resource',
								'value' => '1',
								'compare' => '=='
							)
						)
				);

				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						get_template_part( 'template-parts/content', 'resource-menu-side' );
					}
				} ?>
				<?php wp_reset_query(); ?>
			<?php restore_current_blog(); ?>	
			<a class="mega-menu-button" href="<?php echo get_home_url(); ?>/resources/">View All Resources <img src="<?php bloginfo('stylesheet_directory'); ?>/img/mega-menu-btn-arrow.png" /></a>
		</div>
		<div style="clear: both"></div>
	</div>
	<div id="blog-sub-menu" class="mega-menu max-width">
		<div class="featured">
			<h2>Featured Article</h2>
			<hr>
			<div id="viewpoint-featured">
				<?php switch_to_blog(1); ?>
				    <?php $args = array(
						'posts_per_page' => 1,
						'meta_key'		 => 'featured_post',
						'meta_value'	 => true,
					);

					$the_query = new WP_Query( $args );
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-featured-menu' );
						}
					} ?>
					<?php wp_reset_query(); ?>
				<?php restore_current_blog(); ?>
			</div>
		</div>
		<div class="recent">
			<h2>Recent Articles</h2>
			<hr>
			<?php switch_to_blog(1); ?>
				<?php $args = array(
				    'posts_per_page' => 3,
				    'offset' => 0,
				);

				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						get_template_part( 'template-parts/content', 'viewpoint-side' );
					}
				} ?>
				<?php wp_reset_query(); ?>
			<?php restore_current_blog(); ?>
			<a class="mega-menu-button" href="<?php echo get_home_url(); ?>/viewpoint/">View All Articles <img src="<?php bloginfo('stylesheet_directory'); ?>/img/mega-menu-btn-arrow.png" /></a>
		</div>
		<div style="clear: both"></div>
	</div>