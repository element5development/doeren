<?php /*
TEMPLATE FOR SINGLE CUSTOM POST TYPE "EVENTS"
*/ ?>

<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width full-page-container">

	<div class="page-header max-width" style="background-image: url(<?php echo $src[0]; ?>);">
		<?php if ( get_field('master_head') ) { ?>
		<?php } else { ?>
			<div class="page-header-contents">
				<?php if( get_field('header-logo') ): ?>
					<img src="<?php the_field( 'header-logo' ) ?>" />
				<?php else: ?>
					<div class="event-format"><?php the_field( 'format' ) ?></div>
					<h1 class="page-title"><?php echo get_the_title( $ID ); ?></h1>
					<p class="page-desctiption"><?php the_field( 'page_description' ) ?></p>
				<?php endif; ?>
			</div>
		<?php } ?>
	</div>

	<div id="page-contents-container" class="max-width events-contents">

	<aside id="single-sidebar" class="left widget-area-container">
			<div class="grey-bg">
				<?php if ( get_field( 'format' ) == "webinar" ) { ?>

					<h2>Online Event</h2>

				<?php } else { ?>
				<h2>When & Where</h2>
				<?php } ?>
				<div class="event-date-time">
					<div class="event-main-date">
						<span class="month"><?php echo eo_get_the_start('D, M') ?></span>	
						<span class="day"><?php echo eo_get_the_start('j') ?></span>
						<span class="year"><?php echo eo_get_the_start('Y') ?></span>
					</div>
					<?php if ( ! eo_recurs() ) { ?>
						<div class="event-time">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-clock-blue.svg" />
							<?php if ( eo_get_the_start('j') != eo_get_the_end('j')  ) { ?>
								<?php echo eo_get_the_start('n.j.y') ?> - <?php echo eo_get_the_end('n.j.y') ?>
							<?php } else { ?>
								<?php echo eo_get_the_start('g:i a')." to ".eo_get_the_end('g:i a'); ?> <?php the_field('timezone'); ?>
							<?php } ?>
						</div>
					<?php } ?>
				</div>

				<?php if ( get_field( 'format' ) == "webinar" ) { ?>



				<?php } else { ?>
					<?php the_field('map'); ?>
					<?php $address_array = eo_get_venue_address($event->ID); ?>
					<div class="event-location">
						<span><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-map-pointer-lblue.svg" /><?php the_field( 'venue_name' ) ?><br/><?php the_field( 'city' ) ?>, <?php the_field( 'state' ) ?></span>
					</div>
				<?php } ?>
				<?php 

				?>
				<br/><br/>
				<?php 
				if ( get_field('timezone') == 'PST' ) { //PST +3 hours
					//CONVERT START TIME
					$time_start = eo_get_the_start('c');
					$start_hours = substr($time_start, -14, 2);
					$start_hours = $start_hours + 3; 
					$start_hours = str_pad($start_hours, 2, '0', STR_PAD_LEFT);
					$time_start = substr_replace ( $time_start , $start_hours , 11 , 2 );
					//CONVERT END TIME
					$time_end = eo_get_the_end('c');
					$start_hours = substr($time_end, -14, 2);
					$start_hours = $start_hours + 3; 
					$start_hours = str_pad($start_hours, 2, '0', STR_PAD_LEFT);
					$time_end = substr_replace ( $time_end , $start_hours , 11 , 2 );
				}
				elseif ( get_field('timezone') == 'MST'  ) { //MST +2 hours
					//CONVERT START TIME
					$time_start = eo_get_the_start('c');
					$start_hours = substr($time_start, -14, 2);
					$start_hours = $start_hours + 2; 
					$start_hours = str_pad($start_hours, 2, '0', STR_PAD_LEFT);
					$time_start = substr_replace ( $time_start , $start_hours , 11 , 2 );
					//CONVERT END TIME
					$time_end = eo_get_the_end('c');
					$start_hours = substr($time_end, -14, 2);
					$start_hours = $start_hours + 2; 
					$start_hours = str_pad($start_hours, 2, '0', STR_PAD_LEFT);
					$time_end = substr_replace ( $time_end , $start_hours , 11 , 2 );
				}
				elseif ( get_field('timezone') == 'CST'  ) { //CST +1 hour
					//CONVERT START TIME
					$time_start = eo_get_the_start('c');
					$start_hours = substr($time_start, -14, 2);
					$start_hours = $start_hours + 1; 
					$start_hours = str_pad($start_hours, 2, '0', STR_PAD_LEFT);
					$time_start = substr_replace ( $time_start , $start_hours , 11 , 2 );
					//CONVERT END TIME
					$time_end = eo_get_the_end('c');
					$start_hours = substr($time_end, -14, 2);
					$start_hours = $start_hours + 1; 
					$start_hours = str_pad($start_hours, 2, '0', STR_PAD_LEFT);
					$time_end = substr_replace ( $time_end , $start_hours , 11 , 2 );
				}
				else { //EST +0 hours
					$time_start = eo_get_the_start('c');
					$time_end = eo_get_the_end('c');
				}
				?>

				<span class="addtocalendar atc-style-blue secondary-button">
			        <var class="atc_event">
			            <var class="atc_date_start"><?php echo $time_start ?></var>
			            <var class="atc_date_end"><?php echo $time_end ?></var>
			            <var class="atc_timezone">UTC</var>
			            <var class="atc_title"><?php echo get_the_title( $ID ); ?></var>
			            <var class="atc_description">Event brought to you by Doeren Mayhew</var>
			            <var class="atc_location">
			            	<?php if ( $address_array['address'] == '' ) { ?>
			            		Online
			            	<?php } else { ?>
			            		<?php echo $address_array['address'].' '.$address_array['postcode']; ?>
			            	<?php } ?>
			            </var>
			            <var class="atc_organizer">Doeren Mayhew</var>
			        </var>
			    </span>
				<a class="secondary-button" href="/events/event/">< Back to Events</a>
			</div>
			<?php echo do_shortcode('[widget id="a2a_share_save_widget-2"]'); ?>
		</aside>
		<section id="single-sidebar-contents" class="right">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</section>

		<div style="clear: both"></div>

	</div>

</main>

<?php get_footer(); ?>