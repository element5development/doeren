<?php /*
TEMPLATE FOR SINGLE CUSTOM POST TYPE "LOCATIONS"
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<div class="back-to-parent max-width">
		<a class="back-page" href="/about/locations/">Back to Locations</a>
	</div>

	<?php if ( is_single('195') ) {
	?> 
		<div class="location-slider max-width">
			<?php echo do_shortcode( '[rev_slider alias="troy-location"]' ); ?>
		</div>
	<?php
	} elseif ( is_single('197') ) {
	?> 
		<div class="location-slider max-width">
			<?php echo do_shortcode( '[rev_slider alias="houston-location"]' ); ?>
		</div>
	<?php
	} else {
	?> 
		<div class="location-img"><?php the_post_thumbnail(); ?></div>
	<?php
	} ?>

	<div id="page-contents-container" class="max-width">
		<aside id="single-sidebar" class="left location-info">
			<div class="grey-bg">
				<h2><?php the_title(); ?></h2>
				<div class="address">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-map-pointer-blue.svg" />
					<?php the_field( 'address' ) ?>
				</div>
				<div class="phone">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-blue.svg" />
					<a href="tel:<?php the_field( 'phone' ) ?>"><?php the_field( 'phone' ) ?></a>
				</div>
				<a class="secondary-button" href="/contact">Contact Us</a>
			</div>
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-left-sidebar')) : else : ?>
			<?php endif; ?>  
		</aside>

		<section id="single-sidebar-contents" class="right">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</section>
		<div style="clear: both"></div>
	</div>

</main>

<?php get_footer(); ?>