<?php /*
TEMPLATE FOR SINGLE CUSTOM POST TYPE "RESOURCES"
*/ ?>

<?php get_header(); ?>
<?php
if ($_COOKIE['resource-download'] == 1 && !empty($_COOKIE['resource-Email'])) {
	$person_email = $_COOKIE['resource-Email'];
	$get_resources = $wpdb->get_results( 'SELECT * FROM wp_resources WHERE person_email = '.'"'.$person_email.'"'.' AND is_inactive = 0', OBJECT );
	// echo $person_email;
	// print_r($get_resources);
	// die;
	$total_resources = $get_resources[0]->resources_views;
	// setcookie( 'resources-'.get_the_id(), get_the_title(), 0, COOKIEPATH, COOKIE_DOMAIN, false, false );
	$total_resources .= ';'.get_the_title();
	$wpdb->update( 
		'wp_resources', 
		array( 
			'resources_views' => $total_resources
		), 
		array( 'person_email' => $person_email ),
		array( 
			'%s'
		), 
		array( '%s' ) 
	);
}
?>

<main class="full-width full-page-container">

	<div class="back-to-parent max-width">
		<a class="back-page" href="/resources/">Back to Resources</a>
	</div>

	<section id="single-column-contents" class="max-width resource-post">
		<h1><?php the_title(); ?></h1>
		<div class="resource-featured-image"><?php the_post_thumbnail() ?></div>
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</section>

</main>

<?php get_footer(); ?>