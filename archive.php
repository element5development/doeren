<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR BLOG
*/ ?>

<?php get_header(); ?>

<main class="max-width">

	<?php $term = get_queried_object();?>
	<?php if ( get_field('category_header_image', $term) ) : ?>
		<div class="page-header custom-header max-width">
				<?php if ( get_field('category_title', $term) ) : ?>
					<div class="page-header-contents" style="background: transparent;">
						<h1><?php the_field('category_title', $term); ?></h1>
					</div>
				<?php endif; ?>
			<img src="<?php the_field('category_header_image', $term) ?>" alt="" />
		</div>
	<?php else : ?>
		<div class="page-header max-width" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/blog-bg.png);">
			<div class="page-header-contents" style="background: transparent;">
				<?php if ( get_field('category_title', $term) ) : ?>
					<div class="page-header-contents" style="background: transparent;">
						<h1><?php the_field('category_title', $term); ?></h1>
						<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
					</div>
				<?php else : ?>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/viewpoint-logo.png" />
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div id="page-contents-container" class="max-width">
		<section id="single-sidebar-contents" class="archive-category-feed left max-width">
			<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				
				<?php get_template_part( 'template-parts/content', 'viewpoint-archieves' ); ?>

			<?php endwhile; ?>
			<div style="clear: both"></div>
			<?php the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );
			else : ?>
				<article>
					<h2>No Articles Were Found</h2>
				</article>
				<hr>
				<h2></h2>
				<?php 
					$argss = [ 
						'order' => 'ASC', 
						'posts_per_page' => 5,
				    ];
					query_posts( $argss );
					if ( have_posts() ) : while ( have_posts() ) : the_post();
				?>
							<?php get_template_part( 'template-parts/content', 'viewpoint-archieves' ); ?>
					<?php endwhile; ?><?php endif; ?>
			<?php endif; ?>
		</section>
		<aside id="single-sidebar" class="right widget-area-container">
			<div class="widget categories-widget">
				<h2 class="right-sidebar-title">Categories</h2>
				<ul>
					<li class="top-level has-child">Services
						<ul class="child-categories">
							<?php $child_categories=get_categories(array( 'parent' => 24 )); ?>
							<?php foreach ( $child_categories as $child ) : ?>
								<li><a href="<?php echo get_category_link($child->term_id) ?>"><?php echo $child->cat_name; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</li>
					<li class="top-level has-child">Specializations
						<ul class="child-categories">
							<?php $child_categories=get_categories(array( 'parent' => 23 )); ?>
							<?php foreach ( $child_categories as $child ) : ?>
								<li><a href="<?php echo get_category_link($child->term_id) ?>"><?php echo $child->cat_name; ?></a></li>
								<?php if ( $child->term_id == 40 ) : ?>
									<li><a href="<?php echo get_category_link(701) ?>"><?php echo get_cat_name(701); ?></a></li>
								<?php endif; ?>
							<?php endforeach; ?>
						</ul>
					</li>
					<li class="top-level"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">View All</a></li>
				</ul>
			</div>
			<?php $category = get_queried_object(); ?>
			<?php if( $category->term_id == 701 ) : ?>
				<div class="widget widget_simpleimage">
					<a href="https://doeren.com/regulatory-compliance-viewpoint-subscription/"><img src="https://doeren.com/wp-content/uploads/2019/07/fig-compliance-viewpoint-sidebar-002.jpg" class="attachment-full size-full" alt="fig-compliance-viewpoint-sidebar (002)" srcset="https://doeren.com/wp-content/uploads/2019/07/fig-compliance-viewpoint-sidebar-002.jpg 470w, https://doeren.com/wp-content/uploads/2019/07/fig-compliance-viewpoint-sidebar-002-300x249.jpg 300w" sizes="(max-width: 470px) 85vw, 470px" width="470" height="390"></a>
					<p><a target="" href="https://doeren.com/regulatory-compliance-viewpoint-subscription/" class="dark-button">Subscribe Now</a></p>
				</div>
			<?php else : ?>
				<div id="text-24" class="widget widget_text">
					<h2 class="two-sidebar-right-title">Stay in the Know</h2>			
					<div class="textwidget">
						<p>Join our newsletter</p>
						<a target="" href="https://doeren.com/subscribe/" class="dark-button" rel="noopener noreferrer">Subscribe</a>
					</div>
				</div>
			<?php endif; ?>
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('archieve-sidebar')) : else : ?> 
			<?php endif; ?>  
		</aside>
		<div style="clear: both"></div>
	</div>

</main>

<?php get_footer(); ?>