<?php
/**
 * Template Name: Employee Directory
 *
 * Template file for employee directory
 *
 * @package WordPress
 * @subpackage doeren
 */

get_header(); ?>

<main class="full-width full-page-container">

    <?php get_template_part( 'template-parts/content', 'page-header' ); ?>

    <div id="page-contents-container" class="max-width">

        <section id="single-sidebar-contents" class="right">
    <div id="content">

        <div id="right-column" class="column">
            <div id="banner">
                <div id="banner-header" class="no-banner">
                    <!-- <h1><?php the_title(); ?></h1> -->
                </div>
            </div>

            <div id="page-content" class="directory-page white-bg no-padding">
                <?php
                    $location_tax = get_categories(array(
                        'taxonomy' => 'directory_location',
                        'hide_empty' => 0,
                        'order' => 'DESC'
                    ));

                    foreach($location_tax as $location)
                    {
                        if(!empty($_GET['location_select']))
                        {
                            if($_GET['location_select'] == 'troy' && $location->slug == 'troy-mi')
                            {
                                $locations[$location->slug] = $location->name;
                            }
                            elseif($_GET['location_select'] == 'houston' && $location->slug == 'houston-tx')
                            {
                                $locations[$location->slug] = $location->name;
                            }
                            elseif($_GET['location_select'] == 'florida' && $location->slug == 'ft-lauderdale-fl')
                            {
                                $locations[$location->slug] = $location->name;
                            }
                        }
                        else
                        {
                            $locations[$location->slug] = $location->name;
                        }
                    }

                    foreach($locations as $slug=>$title) :
                ?>

                    <h4 style="margin: 0 0 5px 0;"><?php echo $title; ?></h4>

                    <table class="directory-table" style="margin-bottom: 15px; text-align: left; border-collapse: collapse;">
                        <tr class="odd header">
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                        </tr>

                        <?php
                            $args = array(
                                'post_type' => 'directory',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'directory_location',
                                        'field' => 'slug',
                                        'terms' => $slug
                                    )
                                ),
                                'orderby' => 'meta_value',
                                'meta_key' => '_employee_last_name',
                                'order' => 'ASC',
                                'posts_per_page' => -1
                            );

                            $query = new WP_Query($args);

                            $i = 0; //row counter
                        ?>

                        <?php if ( $query->have_posts() ) while ( $query->have_posts() ) : $query->the_post(); ?>
                            <tr <?php echo (($i = !$i)?'':' class="odd"'); ?>>
                                <td><a href="#"><?php echo get_the_title(); ?></a></td>
                                <td><?php echo get_post_meta($post->ID,'_employee_phone',TRUE); ?></td>
                                <td><a href="mailto:<?php echo get_post_meta($post->ID,'_employee_email',TRUE); ?>"><?php echo get_post_meta($post->ID,'_employee_email',TRUE); ?></a></td>
                            </tr>

                            <?php $i++; ?>
                        <?php endwhile; ?>

                    </table>

                <?php endforeach; ?>
            </div>
        </div>
    </div>
        </section>

        <aside id="single-sidebar" class="left widget-area-container">
            <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-left-sidebar')) : else : ?>
                <p><strong>Widget Ready</strong></p>  
            <?php endif; ?>  
        </aside>

        <div style="clear: both"></div>

    </div>
</main>

<style>
    .directory-table {
        width: 100%;
    }
    .directory-table tr td{
        width: 33.33%;
        border: 1px solid #f1f1f1;
    }
    .directory-table tr th,
    .directory-table tr td{
        border: 1px solid #f1f1f1;
        padding: 5px;
    }
</style>

<?php get_footer(); ?>