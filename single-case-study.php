<?php /*
SINGLE CASE STUDY
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<?php get_template_part( 'template-parts/content', 'page-header' ); ?>

	<div id="valuation-nav" class="secondary-nav max-width">
		<nav>
			<?php wp_nav_menu( array( 'theme_location' => 'valuation-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
			<div style="clear: both"></div>
		</nav>
	</div>

	<div id="page-contents-container" class="max-width">
		<aside id="double-left-sidebar" class="widget-area-container">
			<!--=====================-->
			<!--VIEWPOINT QUERY LOGIC-->
			<!--=====================-->
			<?php //LOGIC
			// any from THIS category? 
				switch_to_blog(1);
				$args = array(
					'category_name' => 'valuation-litigation-support-services',
					'posts_per_page' => 3,
					'post_type' => 'post',
					'orderby' => 'date',
					'order' => 'DESC',
					// Using the date_query to filter posts from the last 6 months
					'date_query' => array(
							array(
									'after' => '6 month ago'
							)
					)
				);
				$the_query = new WP_Query( $args );
				$post_count = $the_query->found_posts;
				restore_current_blog();
			?>
			<?php switch_to_blog(1); //QUERY ?>
			<?php	if ( $post_count > 0 ) { ?>
				<div class="grey-bg">
					<h2>
						<a href="/viewpoint/">Recent Articles</a>
					</h2>
					<?php while ( $the_query->have_posts() ) {
						$the_query->the_post();
						get_template_part( 'template-parts/content', 'viewpoint-side' );
					} ?>
				</div>
			<?php } else { ?>
			<?php 
				$args = array(
					'posts_per_page' => 3,
					'category_name' => 'general',
				);
				$general_query = new WP_Query( $args ); 
			?>
				<div class="grey-bg">
					<h2>
						<a href="/viewpoint/">Recent Articles</a>
					</h2>
					<?php while ( $general_query->have_posts() ) {
						$general_query->the_post();
						get_template_part( 'template-parts/content', 'viewpoint-side' );
					} ?>
				</div>
			<?php } ?>
			<?php wp_reset_query(); ?>
			<?php restore_current_blog(); ?>

			<!--=====================-->
			<!--CASE STUDY QUERY LOGIC-->
			<!--=====================-->
			<?php //LOGIC
			// any from THIS category? 
				switch_to_blog(1);
				$args = array(
					'category_name' => 'valuation-litigation-support-services',
					'posts_per_page' => 3,
					'post_type' => 'case-study',
				);
				$the_query = new WP_Query( $args );
				$post_count = $the_query->found_posts;
				restore_current_blog();
			?>
			<?php switch_to_blog(1); //QUERY ?>
			<?php	if ( $post_count > 0 ) { ?>
				<div class="grey-bg">
					<h2>
						<a href="/case-study/">Case Studies</a>
					</h2>
					<?php while ( $the_query->have_posts() ) {
						$the_query->the_post();
						get_template_part( 'template-parts/content', 'case-study-side' );
					} ?>
				</div>
			<?php } ?>
			<?php wp_reset_query(); ?>
			<?php restore_current_blog(); ?>

			<!--SIDEBAR-->
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-left')) : else : ?>
			<?php endif; ?>
		</aside>

		<section id="double-sidebar-contents">
			<div id="double-sidebar-page-contents">
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>

		<aside id="double-right-sidebar" class="widget-area-container">
			<!--===================-->
			<!--MEMBERS QUERY LOGIC-->
			<!--===================-->
			<?php 
			$posts = get_field('members');

			if( $posts ): ?>
				<div class="grey-bg member-widget">
					<h2>Lead Contacts</h2>
					<div class="my-flipster">
						<ul>
						<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
							<?php setup_postdata($post); ?>
							<?php get_template_part( 'template-parts/content', 'member-side' ); ?>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php else : ?>
				<?php echo do_shortcode('[gravityform id="7" title="false" description="false"]'); ?>
			<?php endif; ?>

			<!--RIGHT SIDEBAR-->
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-right')) : else : ?>
			<?php endif; ?>
			<!--SIDEBAR-->
			<div class="mobile-widget">
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-left')) : else : ?>
				<?php endif; ?>
			</div>
		</aside>
		<div style="clear: both"></div>
	</div>
</main>

<?php get_footer(); ?>