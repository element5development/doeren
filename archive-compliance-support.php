<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR CUSTOM POSTS TYPE "COMPLIANCE SUPPORT"
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

<?php if ( ! is_user_logged_in() ) { // Display WordPress login form: ?>

	<div class="page-header max-width" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/img/compliance-header.jpg');">
		<div class="page-header-contents">
			<div class="header-center">
				<h1 class="page-title">Compliance Help Center</h1>
				<p class="page-desctiption"></p>
			</div>
		</div>
	</div>
	<section id="single-column-contents" class="max-width site<?php echo get_current_blog_id(); ?>">
		<div class="login-form">
			<h3 class="gform_title">Login</h3>

			<?php	$args = array(
				'echo' => false,
				'redirect' => admin_url(), 
				'form_id' => 'loginform-custom',
				'label_username' => __( 'Username' ),
				'label_password' => __( 'Password' ),
				'label_log_in' => __( 'Login' ),
				'remember' => false
				);

				$form = wp_login_form( $args );

				//add the placeholders
				$form = str_replace('name="log"', 'name="log" placeholder="Username"', $form);
				$form = str_replace('name="pwd"', 'name="pwd" placeholder="Password"', $form);

				echo $form; ?>

				<p><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>">Lost your password?</a></p>

				<!-- <p>New User? <a href="/compliance-user-registration/">Register</a></p> -->
		</div>
	</section>

<?php	} else { // If logged in: ?>

	<div class="page-header max-width" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/img/compliance-header.jpg');">
		<div class="page-header-contents">
			<div class="header-center">
				<h1 class="page-title">Compliance Help Center</h1>
				<p class="page-desctiption"></p>
				<!-- <form role="search" method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
					<label for="search">Search</label>
					<input type="search" id="s" name="s" value="" placeholder="Enter your question here" />
					<input type="hidden" name="post_type" value="compliance-support" />
					<input type="submit" value="search" id="searchsubmit" />
				</form> -->
			</div>
		</div>
	</div>

	<!-- <div id="tax-nav" class="secondary-nav max-width service-navigation">
		<nav>
			<?php $field = get_field_object('categories'); ?>
			<?php if( $field['choices'] ): ?>

			<div class="menu-tax-container">
				<ul id="menu-tax" class="menu filter-button-group">
					<li class="menu-item">
						<button class="is-active" data-filter="*">All Categories</button>
					</li>

					<?php foreach( $field['choices'] as $value => $label ): ?>

					<?php $arr = preg_split("/[\s\/]+/", $label ); ?>
					<li class="menu-item">
						<button data-filter=".<?php echo $arr[0]; ?>"><?php echo $label; ?></button>
					</li>

					<?php endforeach; ?>

				</ul>
			</div>

			<?php endif; ?>
			<div style="clear: both"></div>
		</nav>
	</div> -->

	<div id="page-contents-container" class="max-width">

		<aside id="double-left-sidebar" class="widget-area-container">
			<!--=====================-->
			<!--EVENTS QUERY LOGIC-->
			<!--=====================-->
			<?php //LOGIC
				// any from THIS category? 
				switch_to_blog(1);
				$args = array(
					'posts_per_page' => 3,
					'post_type' => 'event',
					'event-category' => 'regulatory-compliance-services-for-credit-unions',
				);
				$the_query = new WP_Query( $args );
				$post_count = $the_query->found_posts;
				restore_current_blog();
			?>
			<?php switch_to_blog(1); //QUERY ?>
			<?php	if ( $post_count > 0 ) { ?>
			<div class="grey-bg">
				<h2>
					<a href="/events/event/">Upcoming Events</a>
				</h2>
				<?php while ( $the_query->have_posts() ) {
						$the_query->the_post();
						get_template_part( 'template-parts/content', 'event-side' );
					} ?>
			</div>
			<?php } ?>
			<?php wp_reset_query(); ?>
			<?php restore_current_blog(); ?>

			<!--=====================-->
			<!--VIEWPOINT QUERY LOGIC-->
			<!--=====================-->
			<?php //LOGIC
			// any from THIS category? 
				$page_slug = 'regulatory-compliance-services-for-credit-unions';
				switch_to_blog(1);
				$args = array(
					'category_name' => $page_slug,
					'posts_per_page' => 3,
					'post_type' => 'post',
					'orderby' => 'date',
					'order' => 'DESC',
					// Using the date_query to filter posts from the last 6 months
					'date_query' => array(
							array(
									'after' => '6 month ago'
							)
					)
				);
				$the_query = new WP_Query( $args );
				$post_count = $the_query->found_posts;
				restore_current_blog();
			?>
			<?php switch_to_blog(1); //QUERY ?>
			<?php	if ( $post_count > 0 ) { ?>
			<div class="grey-bg">
				<h2>
					<a href="/category/specializations/financial-institutions/credit-unions/regulatory-compliance-services-for-credit-unions/">Recent Articles</a>
				</h2>
				<?php while ( $the_query->have_posts() ) {
					$the_query->the_post();
					get_template_part( 'template-parts/content', 'viewpoint-side' );
				} ?>
			</div>
			<?php } ?>
			<?php wp_reset_query(); ?>
			<?php restore_current_blog(); ?>
		</aside>

		<section id="double-sidebar-contents">
			<div id="double-sidebar-page-contents">

			<?php echo do_shortcode('[gravityform id="60" title="true" description="false"]'); ?>

			<?php if( have_rows('compliance_repeater', 'options') ): ?>
				<?php while( have_rows('compliance_repeater', 'options') ) : the_row(); ?>

					<div class="compliance-card">
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
						<?php $link = get_sub_field('link');
						if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a class="primary-button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					</div>

				<?php endwhile; ?>
			<?php endif; ?>

			<!-- <?php $field = get_field_object('categories'); ?>
			<?php if( $field['choices'] ): ?>
				<?php foreach( $field['choices'] as $value => $label ): ?>
					<?php $arr = preg_split("/[\s\/]+/", $label ); ?>
					<div class="question-item <?php echo $arr[0]; ?>">
						<h3><?php echo $label; ?></h3>

						<?php 

						$args = array(
							'posts_per_page'	=> -1,
							'post_type'		=> 'compliance-support',
							'meta_query' => array(
								array(
									'key'     => 'categories',
									'value'   => $label,
									'compare' => 'LIKE'
								)
							)
						);

						// query
						$the_query = new WP_Query( $args ); ?>

						<?php if( $the_query->have_posts() ): ?>
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<p><a href="<?php the_permalink(); ?>"><?php the_field('question'); ?></a></p>
							<?php endwhile; ?>
						<?php endif; ?>

					</div>
				<?php endforeach; ?>
			<?php endif; ?> -->


			</div>
		</section>

		<aside id="double-right-sidebar" class="widget-area-container">
			<?php	wp_loginout( home_url() ); // Display "Log Out" link. ?>
			<!--===================-->
			<!--MEMBERS QUERY LOGIC-->
			<!--===================-->
			<?php 
			$posts = get_field('members', 'options');

			if( $posts ): ?>
				<div class="grey-bg member-widget">
					<h2>Compliance Team</h2>
					<div class="my-flipster">
						<ul>
						<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
							<?php setup_postdata($post); ?>
							<?php get_template_part( 'template-parts/content', 'member-side' ); ?>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>

			<!--======================-->
			<!--SIGN UP TO BE NOTIFIED-->
			<!--======================-->
			<div id="text-24" class="widget widget_text">
				<h2 class="two-sidebar-right-title">Stay in the Know</h2>			
				<div class="textwidget">
					<p>Get our Compliance VIEWpoint updates sent to your inbox.</p>
					<a target="" href="<?php echo get_home_url(); ?>/subscribe/" class="dark-button" rel="noopener noreferrer">Subscribe</a>
					<!-- <a href="<?php echo get_home_url(); ?>/compliance-insights/">View Past Newsletters</a> -->
				</div>
			</div>
		</aside>

		<div style="clear: both"></div>

	</div>
<?php	} ?>

</main>

<?php get_footer(); ?>